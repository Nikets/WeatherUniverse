#!/bin/bash

# Загрузка файла локализации
if [ -e "$(pwd)/locales/frontend/$wu_language.ini" ]
then
    . "$(pwd)/locales/frontend/$wu_language.ini"
else
    if [ -e "$(dirname "$0")/locales/frontend/$wu_language.ini" ]
    then
        . "$(dirname "$0")/locales/frontend/$wu_language.ini"
    else
        if [ -e "$HOME/.config/WeatherUniverse/locales/frontend/$wu_language.ini" ]
        then
            . "$HOME/.config/WeatherUniverse/locales/frontend/$wu_language.ini"
        else
            if [ -e "$HOME/.local/share/WeatherUniverse/locales/frontend/$wu_language.ini" ]
            then
                . "$HOME/.local/share/WeatherUniverse/locales/frontend/$wu_language.ini"
            fi
        fi
    fi
fi

# Присваивание названий единиц измерения
units() {
# $precip_unit $pressure_unit
    if [[ $temp_unit == "celsius" ]]
    then
        temp_unit_text=${lng_str_temp_unit_celsius:-"°C"}
    else
        if [[ $temp_unit == "fahrenheit" ]]
        then
            temp_unit_text=${lng_str_temp_unit_fahrenheit:-"°F"}
        fi
    fi

    if [[ $wind_unit == "ms" ]]
    then
        wind_unit_text=${lng_str_wind_unit_ms:-"m/s"}
    else
        if [[ $wind_unit == "kmh" ]]
        then
            wind_unit_text=${lng_str_wind_unit_kmh:-"km/h"}
        else
            if [[ $wind_unit == "mph" ]]
            then
                wind_unit_text=${lng_str_wind_unit_mph:-"mph"}
            else
                if [[ $wind_unit == "kn" ]]
                then
                    wind_unit_text=${lng_str_wind_unit_kn:-"kn"}
                fi
            fi
        fi
    fi

    if [[ $precip_unit == "mm" ]]
    then
        precip_unit_text=${lng_str_precip_unit_mm:-"mm"}
    else
        if [[ $precip_unit == "inch" ]]
        then
            precip_unit_text=${lng_str_precip_unit_inch:-"inch"}
        fi
    fi

     if [[ $pressure_unit == "hpa" ]]
    then
        pressure_unit_text=${lng_str_pressure_unit_hpa:-"hPa"}
    else
        if [[ $pressure_unit == "mmhg" ]]
        then
            pressure_unit_text=${lng_str_pressure_unit_mmhg:-"mmHg"}
        fi
    fi
}

# Конвертирование направления ветра из градусов в текст
wind_direction_convertor() {
    wind_direction_number_1=(11 34 56 79 101 124 146 169 191 214 236 259 281 304)
    wind_direction_number_2=(34 56 79 101 124 146 169 191 214 236 259 281 304 326)
    wind_direction_text_array=("${lng_str_direction_nne:-"NNE"}" "${lng_str_direction_ne:-"NE"}" "${lng_str_direction_ene:-"ENE"}" "${lng_str_direction_e:-"E"}" "${lng_str_direction_ese:-"ESE"}" "${lng_str_direction_se:-"SE"}" "${lng_str_direction_sse:-"SSE"}" "${lng_str_direction_s:-"S"}" "${lng_str_direction_ssw:-"SSW"}" "${lng_str_direction_sw:-"SW"}" "${lng_str_direction_wsw:-"WSW"}" "${lng_str_direction_w:-"W"}" "${lng_str_direction_wnw:-"WNW"}" "${lng_str_direction_nw:-"NW"}")

    if (( 0 <= $wind_direction && $wind_direction <= 11 ))
    then
        wind_direction_text="${lng_str_direction_n:-"N"}"
    else
        if (( 326 < $wind_direction && $wind_direction < 349 ))
        then
            wind_direction_text="${lng_str_direction_nnw:-"NNW"}"
        else
            if (( 349 <= $wind_direction && $wind_direction <= 360 ))
            then
                wind_direction_text="${lng_str_direction_n:-"N"}"
            else
                for ((wd=0;wd<14;wd++))
                do
                    if ((${wind_direction_number_1[$wd]} < $wind_direction && $wind_direction <= ${wind_direction_number_2[$wd]}))
                    then
                        wind_direction_text=${wind_direction_text_array[$wd]}
                    fi
                done
            fi
        fi
    fi
}

# Open-Meteo провайдер
open_meteo() {
    # Конвертер числового значения погоды в текстовое
    open_meteo_weather_code_converter() {
        # Объявление массивов для числовых значений погоды
        open_meteo_weathercode_number=(0 1 2 3 45 48 51 53 55 56 57 61 63 65 66 67 71 73 75 77 80 81 82 85 86 95 96 99)

        # Объявление массивов для текстовых значений погоды
        open_meteo_weathercode_text_array=("${lng_str_clear_sky:-"Clear sky"}" "${lng_str_mainly_clear:-"Mainly clear"}" "${lng_str_partly_cloudy:-"Partly cloudy"}" "${lng_str_overcast:-"Overcast"}" "${lng_str_fog:-"Fog"}" "${lng_str_depositing_rime_fog:-"Depositing rime fog"}" "${lng_str_light_drizzle:-"Light drizzle"}" "${lng_str_moderate_drizzle:-"Moderate drizzle"}" "${lng_str_dense_intensity_drizzle:-"Dense intensity drizzle"}" "${lng_str_light_freezing_drizzle:-"Light freezing drizzle"}" "${lng_str_dense_intensity_freezing_drizzle:-"Dense intensity freezing drizzle"}" "${lng_str_slight_rain:-"Slight rain"}" "${lng_str_moderate_rain:-"Moderate rain"}" "${lng_str_heavy_intensity_rain:-"Heavy intensity rain"}" "${lng_str_light_freezing_rain:-"Light freezing rain"}" "${lng_str_heavy_intensity_freezing_rain:-"Heavy intensity freezing rain"}" "${lng_str_slight_snowfall:-"Slight snowfall"}" "${lng_str_moderate_snowfall:-"Moderate snowfall"}" "${lng_str_heavy_intensity_snowfall:-"Heavy intensity snowfall"}" "${lng_str_snow_grains:-"Snow grains"}" "${lng_str_slight_rain_showers:-"Slight rain showers"}" "${lng_str_moderate_rain_showers:-"Moderate rain showers"}" "${lng_str_violent_rain_showers:-"Violent rain showers"}" "${lng_str_slight_snow_showers:-"Slight snow showers"}" "${lng_str_heavy_snow_showers:-"Heavy snow showers"}" "${lng_str_thunderstorm:-"Thunderstorm"}" "${lng_str_thunderstorm_with_slight_hail:-"Thunderstorm with slight hail"}" "${lng_str_thunderstorm_with_heavy_hail:-"Thunderstorm with heavy hail"}")

        # Конвертация числового кода погоды в текстовый
        for om_wc in ${!open_meteo_weathercode_number[*]}
        do
            if [[ $open_meteo_weathercode == ${open_meteo_weathercode_number[$om_wc]} ]]
            then
                open_meteo_weathercode_text=${open_meteo_weathercode_text_array[$om_wc]}
            fi
        done
    }

    # Текущая погода Open-Meteo
    open_meteo_current_weather() {
        open_meteo_current_time_get() {
            open_meteo_current_time_output=" | ${lng_str_time:-"Time:"} $(date -d @${open_meteo_current_weather_array[4]:-0})"
        }

        open_meteo_current_temperature_get() {
            open_meteo_current_temperature_output=" | ${lng_str_temperature:-"Temperature:"} ${open_meteo_current_weather_array[0]:-0} $temp_unit_text"
        }

        open_meteo_current_weathercode_get() {
            # Конвертация значения текущей погоды
            open_meteo_weathercode=${open_meteo_current_weather_array[3]:-0}
            open_meteo_weather_code_converter
            open_meteo_current_weathercode_output=" | ${lng_str_weather:-"Weather:"} $open_meteo_weathercode_text"
        }

        open_meteo_current_wind_direction_get() {
            # Конвертация направления ветра для текущей погоды
            wind_direction=${open_meteo_current_weather_array[2]:-0}
            wind_direction_convertor
            open_meteo_current_wind_direction_output=" | ${lng_str_wind_direction:-"Wind Direction:"} $wind_direction_text"
        }

        open_meteo_current_wind_speed_get() {
            open_meteo_current_wind_speed_output=" | ${lng_str_wind_speed:-"Wind Speed:"} ${open_meteo_current_weather_array[1]:-0} ${lng_str_wind_unit_ms:-"m/s"}"
        }

        # Включенные опции для почасового прогноза погоды
        open_meteo_current_options=($open_meteo_current_time_enabled $open_meteo_current_temperature_enabled $open_meteo_current_weathercode_enabled $open_meteo_current_wind_direction_enabled $open_meteo_current_wind_speed_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        open_meteo_current_options_vallues=(open_meteo_current_time_get open_meteo_current_temperature_get open_meteo_current_weathercode_get open_meteo_current_wind_direction_get open_meteo_current_wind_speed_get)

        for open_meteo_current in ${!open_meteo_current_options[*]}
        do
            if [[ ${open_meteo_current_options[$open_meteo_current]} == "true" ]]
            then
                open_meteo_current_options_array+=(${open_meteo_current_options_vallues[$open_meteo_current]})
            fi
        done

        # Обработка включенных опций
        for open_meteo_current_options_array_n in ${!open_meteo_current_options_array[*]}
        do
            ${open_meteo_current_options_array[$open_meteo_current_options_array_n]}
        done

        # Вывод текущей погоды
        echo "|#| Open-Meteo | ${lng_str_current_weather:-"Current weather"}$open_meteo_current_temperature_output$open_meteo_current_wind_speed_output$open_meteo_current_wind_direction_output$open_meteo_current_weathercode_output$open_meteo_current_time_output"
    }

    # Прогноз по часам Open-Meteo
    open_meteo_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        open_meteo_hourly_time_get() {
            open_meteo_hourly_time_output+=(" | ${lng_str_time:-"Time:"} $(date -d @${open_meteo_hourly_time[$h]:-0} +"%d/%m %H:%M")")
        }

        open_meteo_hourly_temperature_2m_get() {
            open_meteo_hourly_temperature_2m_output+=(" | ${lng_str_temperature_2m:-"Temperature (2m):"} ${open_meteo_hourly_temperature_2m[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_relativehumidity_2m_get() {
            open_meteo_hourly_relativehumidity_2m_output+=(" | ${lng_str_relativehumidity_2m:-"Relative Humidity (2m):"} ${open_meteo_hourly_relativehumidity_2m[$h]:-0} %")
        }

        open_meteo_hourly_dewpoint_2m_get() {
            open_meteo_hourly_dewpoint_2m_output+=(" | ${lng_str_dewpoint_2m:-"Dewpoint (2m):"} ${open_meteo_hourly_dewpoint_2m[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_apparent_temperature_get() {
            open_meteo_hourly_apparent_temperature_output+=(" | ${lng_str_apparent_temperature:-"Apparent Temperature:"} ${open_meteo_hourly_apparent_temperature[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_pressure_msl_get() {
            open_meteo_hourly_pressure_msl_output+=(" | ${lng_str_pressure_msl:-"Sealevel Pressure:"} ${open_meteo_hourly_pressure_msl[$h]:-0} ${lng_str_pressure_unit_hpa:-"hPa"}")
        }

        open_meteo_hourly_surface_pressure_get() {
            open_meteo_hourly_surface_pressure_output+=(" | ${lng_str_surface_pressure:-"Surface Pressure:"} ${open_meteo_hourly_surface_pressure[$h]:-0} ${lng_str_pressure_unit_hpa:-"hPa"}")
        }

        open_meteo_hourly_precipitation_get() {
            open_meteo_hourly_precipitation_output+=(" | ${lng_str_precipitation_rain_showers_snow:-"Precipitation (rain + showers + snow):"} ${open_meteo_hourly_precipitation[$h]:-0} $precip_unit_text")
        }

        open_meteo_hourly_rain_get() {
            open_meteo_hourly_rain_output+=(" | ${lng_str_rain_text:-"Rain:"} ${open_meteo_hourly_rain[$h]:-0} $precip_unit_text")
        }

        open_meteo_hourly_showers_get() {
            open_meteo_hourly_showers_output+=(" | ${lng_str_showers:-"Showers:"} ${open_meteo_hourly_showers[$h]:-0} $precip_unit_text")
        }

        open_meteo_hourly_snowfall_get() {
            open_meteo_hourly_snowfall_output+=(" | ${lng_str_snowfall:-"Snowfall:"} ${open_meteo_hourly_snowfall[$h]:-0} ${lng_str_unit_cm:-"cm"}")
        }

        open_meteo_hourly_weathercode_get() {
            open_meteo_weathercode=${open_meteo_hourly_weathercode[$h]:-0}
            open_meteo_weather_code_converter
            open_meteo_hourly_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $open_meteo_weathercode_text")
        }

        open_meteo_hourly_snow_depth_get() {
            open_meteo_hourly_snow_depth_output+=(" | ${lng_str_snow_depth:-"Snow Depth:"} ${open_meteo_hourly_snow_depth[$h]:-0} ${lng_str_unit_m:-"m"}")
        }

        open_meteo_hourly_freezinglevel_height_get() {
            open_meteo_hourly_freezinglevel_height_output+=(" | ${lng_str_freezinglevel_height:-"Freezinglevel Height:"} ${open_meteo_hourly_freezinglevel_height[$h]:-0} ${lng_str_unit_m:-"m"}")
        }

        open_meteo_hourly_cloudcover_get() {
            open_meteo_hourly_cloudcover_output+=(" | ${lng_str_cloudcover_total:-"Cloud Cover (total):"} ${open_meteo_hourly_cloudcover[$h]:-0} %")
        }

        open_meteo_hourly_cloudcover_low_get() {
            open_meteo_hourly_cloudcover_low_output+=(" | ${lng_str_cloudcover_low:-"Cloud Cover (low):"} $open_meteo_hourly_cloudcover_low %")
        }

        open_meteo_hourly_cloudcover_mid_get() {
            open_meteo_hourly_cloudcover_mid_output+=(" | ${lng_str_cloudcover_mid:-"Cloud Cover (mid):"} ${open_meteo_hourly_cloudcover_mid[$h]:-0} %")
        }

        open_meteo_hourly_cloudcover_high_get() {
            open_meteo_hourly_cloudcover_high_output+=(" | ${lng_str_cloudcover_high:-"Cloud Cover (high):"} ${open_meteo_hourly_cloudcover_high[$h]:-0} %")
        }

        open_meteo_hourly_shortwave_radiation_get() {
            open_meteo_hourly_shortwave_radiation_output+=(" | ${lng_str_shortwave_radiation:-"Shortwave Solar Radiation:"} ${open_meteo_hourly_shortwave_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        open_meteo_hourly_direct_radiation_get() {
            open_meteo_hourly_direct_radiation_output+=(" | ${lng_str_direct_radiation:-"Direct Solar Radiation:"} ${open_meteo_hourly_direct_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        open_meteo_hourly_diffuse_radiation_get() {
            open_meteo_hourly_diffuse_radiation_output+=(" | ${lng_str_diffuse_radiation:-"Diffuse Solar Radiation:"} ${open_meteo_hourly_diffuse_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        open_meteo_hourly_direct_normal_irradiance_get() {
            open_meteo_hourly_direct_normal_irradiance_output+=(" | ${lng_str_direct_normal_irradiance:-"Direct Normal Irradiance (DNI):"} ${open_meteo_hourly_direct_normal_irradiance[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        open_meteo_hourly_evapotranspiration_get() {
            open_meteo_hourly_evapotranspiration_output+=(" | ${lng_str_evapotranspiration:-"Evapotranspiration:"} ${open_meteo_hourly_evapotranspiration[$h]:-0} $precip_unit_text")
        }

        open_meteo_hourly_et0_fao_evapotranspiration_get() {
            open_meteo_hourly_et0_fao_evapotranspiration_output+=(" | ${lng_str_et0_fao_evapotranspiration:-"Reference Evapotranspiration (ET₀):"} ${open_meteo_hourly_et0_fao_evapotranspiration[$h]:-0} $precip_unit_text")
        }

        open_meteo_hourly_vapor_pressure_deficit_get() {
            open_meteo_hourly_vapor_pressure_deficit_output+=(" | ${lng_str_vapor_pressure_deficit:-"Vapor Pressure Deficit:"} ${open_meteo_hourly_vapor_pressure_deficit[$h]:-0} ${lng_str_pressure_unit_kpa:-"kPa"}")
        }

        open_meteo_hourly_windspeed_10m_get() {
            open_meteo_hourly_windspeed_10m_output+=(" | ${lng_str_windspeed_10m:-"Wind Speed (10 m):"} ${open_meteo_hourly_windspeed_10m[$h]:-0} $wind_unit_text")
        }

        open_meteo_hourly_windspeed_80m_get() {
            open_meteo_hourly_windspeed_80m_output+=(" | ${lng_str_windspeed_80m:-"Wind Speed (80 m):"} ${open_meteo_hourly_windspeed_80m[$h]:-0} $wind_unit_text")
        }

        open_meteo_hourly_windspeed_120m_get() {
            open_meteo_hourly_windspeed_120m_output+=(" | ${lng_str_windspeed_120m:-"Wind Speed (120 m):"} ${open_meteo_hourly_windspeed_120m[$h]:-0} $wind_unit_text")
        }

        open_meteo_hourly_windspeed_180m_get() {
            open_meteo_hourly_windspeed_180m_output+=(" | ${lng_str_windspeed_180m:-"Wind Speed (180 m):"} ${open_meteo_hourly_windspeed_180m[$h]:-0} $wind_unit_text")
        }

        open_meteo_hourly_winddirection_10m_get() {
            wind_direction=${open_meteo_hourly_winddirection_10m[$h]:-0}
            wind_direction_convertor
            open_meteo_hourly_winddirection_10m_output+=(" | ${lng_str_winddirection_10m:-"Wind Direction (10 m):"} $wind_direction_text")
        }

        open_meteo_hourly_winddirection_80m_get() {
            wind_direction=${open_meteo_hourly_winddirection_80m[$h]:-0}
            wind_direction_convertor
            open_meteo_hourly_winddirection_80m_output+=(" | ${lng_str_winddirection_80m:-"Wind Direction (80 m):"} $wind_direction_text")
        }

        open_meteo_hourly_winddirection_120m_get() {
            wind_direction=${open_meteo_hourly_winddirection_120m[$h]:-0}
            wind_direction_convertor
            open_meteo_hourly_winddirection_120m_output+=(" | ${lng_str_winddirection_120m:-"Wind Direction (120 m):"} $wind_direction_text")
        }

        open_meteo_hourly_winddirection_180m_get() {
            wind_direction=${open_meteo_hourly_winddirection_180m[$h]:-0}
            wind_direction_convertor
            open_meteo_hourly_winddirection_180m_output+=(" | ${lng_str_winddirection_180m:-"Wind Direction (180 m):"} $wind_direction_text")
        }

        open_meteo_hourly_windgusts_10m_get() {
            open_meteo_hourly_windgusts_10m_output+=(" | ${lng_str_windgusts_10m:-"Wind Gusts (10 m):"} ${open_meteo_hourly_windgusts_10m[$h]:-0} $wind_unit_text")
        }

        open_meteo_hourly_soil_temperature_0cm_get() {
            open_meteo_hourly_soil_temperature_0cm_output+=(" | ${lng_str_soil_temperature_0cm:-"Soil Temperature (0 cm):"} ${open_meteo_hourly_soil_temperature_0cm[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_soil_temperature_6cm_get() {
            open_meteo_hourly_soil_temperature_6cm_output+=(" | ${lng_str_soil_temperature_6cm:-"Soil Temperature (6 cm):"} ${open_meteo_hourly_soil_temperature_6cm[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_soil_temperature_18cm_get() {
            open_meteo_hourly_soil_temperature_18cm_output+=(" | ${lng_str_soil_temperature_18cm:-"Soil Temperature (18 cm):"} ${open_meteo_hourly_soil_temperature_18cm[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_soil_temperature_54cm_get() {
            open_meteo_hourly_soil_temperature_54cm_output+=(" | ${lng_str_soil_temperature_54cm:-"Soil Temperature (54 cm):"} ${open_meteo_hourly_soil_temperature_54cm[$h]:-0} $temp_unit_text")
        }

        open_meteo_hourly_soil_moisture_0_1cm_get() {
            open_meteo_hourly_soil_moisture_0_1cm_output+=(" | ${lng_str_soil_moisture_0_1cm:-"Soil Moisture (0-1 cm):"} ${open_meteo_hourly_soil_moisture_0_1cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        open_meteo_hourly_soil_moisture_1_3cm_get() {
            open_meteo_hourly_soil_moisture_1_3cm_output+=(" | ${lng_str_soil_moisture_1_3cm:-"Soil Moisture (1-3 cm):"} ${open_meteo_hourly_soil_moisture_1_3cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        open_meteo_hourly_soil_moisture_3_9cm_get() {
            open_meteo_hourly_soil_moisture_3_9cm_output+=(" | ${lng_str_soil_moisture_3_9cm:-"Soil Moisture (3-9 cm):"} ${open_meteo_hourly_soil_moisture_3_9cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        open_meteo_hourly_soil_moisture_9_27cm_get() {
            open_meteo_hourly_soil_moisture_9_27cm_output+=(" | ${lng_str_soil_moisture_9_27cm:-"Soil Moisture (9-27 cm):"} ${open_meteo_hourly_soil_moisture_9_27cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        open_meteo_hourly_soil_moisture_27_81cm_get() {
            open_meteo_hourly_soil_moisture_27_81cm_output+=(" | ${lng_str_soil_moisture_27_81cm:-"Soil Moisture (27-81 cm):"} ${open_meteo_hourly_soil_moisture_27_81cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        # Включенные опции для почасового прогноза погоды
        open_meteo_hourly_options=($open_meteo_hourly_time_enabled $open_meteo_hourly_temperature_2m_enabled $open_meteo_hourly_relativehumidity_2m_enabled $open_meteo_hourly_dewpoint_2m_enabled $open_meteo_hourly_apparent_temperature_enabled $open_meteo_hourly_pressure_msl_enabled $open_meteo_hourly_surface_pressure_enabled $open_meteo_hourly_precipitation_enabled $open_meteo_hourly_rain_enabled $open_meteo_hourly_showers_enabled $open_meteo_hourly_snowfall_enabled $open_meteo_hourly_weathercode_enabled $open_meteo_hourly_snow_depth_enabled $open_meteo_hourly_freezinglevel_height_enabled $open_meteo_hourly_cloudcover_enabled $open_meteo_hourly_cloudcover_low_enabled $open_meteo_hourly_cloudcover_mid_enabled $open_meteo_hourly_cloudcover_high_enabled $open_meteo_hourly_shortwave_radiation_enabled $open_meteo_hourly_direct_radiation_enabled $open_meteo_hourly_diffuse_radiation_enabled $open_meteo_hourly_direct_normal_irradiance_enabled $open_meteo_hourly_evapotranspiration_enabled $open_meteo_hourly_et0_fao_evapotranspiration_enabled $open_meteo_hourly_vapor_pressure_deficit_enabled $open_meteo_hourly_windspeed_10m_enabled $open_meteo_hourly_windspeed_80m_enabled $open_meteo_hourly_windspeed_120m_enabled $open_meteo_hourly_windspeed_180m_enabled $open_meteo_hourly_winddirection_10m_enabled $open_meteo_hourly_winddirection_80m_enabled $open_meteo_hourly_winddirection_120m_enabled $open_meteo_hourly_winddirection_180m_enabled $open_meteo_hourly_windgusts_10m_enabled $open_meteo_hourly_soil_temperature_0cm_enabled $open_meteo_hourly_soil_temperature_6cm_enabled $open_meteo_hourly_soil_temperature_18cm_enabled $open_meteo_hourly_soil_temperature_54cm_enabled $open_meteo_hourly_soil_moisture_0_1cm_enabled $open_meteo_hourly_soil_moisture_1_3cm_enabled $open_meteo_hourly_soil_moisture_3_9cm_enabled $open_meteo_hourly_soil_moisture_9_27cm_enabled $open_meteo_hourly_soil_moisture_27_81cm_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        open_meteo_hourly_options_vallues=(open_meteo_hourly_time_get open_meteo_hourly_temperature_2m_get open_meteo_hourly_relativehumidity_2m_get open_meteo_hourly_dewpoint_2m_get open_meteo_hourly_apparent_temperature_get open_meteo_hourly_pressure_msl_get open_meteo_hourly_surface_pressure_get open_meteo_hourly_precipitation_get open_meteo_hourly_rain_get open_meteo_hourly_showers_get open_meteo_hourly_snowfall_get open_meteo_hourly_weathercode_get open_meteo_hourly_snow_depth_get open_meteo_hourly_freezinglevel_height_get open_meteo_hourly_cloudcover_get open_meteo_hourly_cloudcover_low_get open_meteo_hourly_cloudcover_mid_get open_meteo_hourly_cloudcover_high_get open_meteo_hourly_shortwave_radiation_get open_meteo_hourly_direct_radiation_get open_meteo_hourly_diffuse_radiation_get open_meteo_hourly_direct_normal_irradiance_get open_meteo_hourly_evapotranspiration_get open_meteo_hourly_et0_fao_evapotranspiration_get open_meteo_hourly_vapor_pressure_deficit_get open_meteo_hourly_windspeed_10m_get open_meteo_hourly_windspeed_80m_get open_meteo_hourly_windspeed_120m_get open_meteo_hourly_windspeed_180m_get open_meteo_hourly_winddirection_10m_get open_meteo_hourly_winddirection_80m_get open_meteo_hourly_winddirection_120m_get open_meteo_hourly_winddirection_180m_get open_meteo_hourly_windgusts_10m_get open_meteo_hourly_soil_temperature_0cm_get open_meteo_hourly_soil_temperature_6cm_get open_meteo_hourly_soil_temperature_18cm_get open_meteo_hourly_soil_temperature_54cm_get open_meteo_hourly_soil_moisture_0_1cm_get open_meteo_hourly_soil_moisture_1_3cm_get open_meteo_hourly_soil_moisture_3_9cm_get open_meteo_hourly_soil_moisture_9_27cm_get open_meteo_hourly_soil_moisture_27_81cm_get)

        for om_ho in ${!open_meteo_hourly_options[*]}
        do
            if [[ ${open_meteo_hourly_options[$om_ho]} == "true" ]]
            then
                open_meteo_hourly_options_array+=(${open_meteo_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for open_meteo_hourly_options_array_n in ${!open_meteo_hourly_options_array[*]}
        do
            for ((h=0;h<$open_meteo_hours;h++))
            do
                ${open_meteo_hourly_options_array[$open_meteo_hourly_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по часам
        for ((hr=0;hr<$open_meteo_hours;hr++))
        do
            # Вывод почасового прогноза только в том случаи, если время прогноза больше текущего времени
            if (( ${open_meteo_hourly_time[$hr]:-0} > $(date +%s) ))
            then
                echo "|#| Open-Meteo${open_meteo_hourly_time_output[$hr]}${open_meteo_hourly_temperature_2m_output[$hr]}${open_meteo_hourly_relativehumidity_2m_output[$hr]}${open_meteo_hourly_dewpoint_2m_output[$hr]}${open_meteo_hourly_apparent_temperature_output[$hr]}${open_meteo_hourly_pressure_msl_output[$hr]}${open_meteo_hourly_surface_pressure_output[$hr]}${open_meteo_hourly_precipitation_output[$hr]}${open_meteo_hourly_rain_output[$hr]}${open_meteo_hourly_showers_output[$hr]}${open_meteo_hourly_snowfall_output[$hr]}${open_meteo_hourly_weathercode_output[$hr]}${open_meteo_hourly_snow_depth_output[$hr]}${open_meteo_hourly_freezinglevel_height_output[$hr]}${open_meteo_hourly_cloudcover_output[$hr]}${open_meteo_hourly_cloudcover_low_output[$hr]}${open_meteo_hourly_cloudcover_mid_output[$hr]}${open_meteo_hourly_cloudcover_high_output[$hr]}${open_meteo_hourly_shortwave_radiation_output[$hr]}${open_meteo_hourly_direct_radiation_output[$hr]}${open_meteo_hourly_diffuse_radiation_output[$hr]}${open_meteo_hourly_direct_normal_irradiance_output[$hr]}${open_meteo_hourly_evapotranspiration_output[$hr]}${open_meteo_hourly_et0_fao_evapotranspiration_output[$hr]}${open_meteo_hourly_vapor_pressure_deficit_output[$hr]}${open_meteo_hourly_windspeed_10m_output[$hr]}${open_meteo_hourly_windspeed_80m_output[$hr]}${open_meteo_hourly_windspeed_120m_output[$hr]}${open_meteo_hourly_windspeed_180m_output[$hr]}${open_meteo_hourly_winddirection_10m_output[$hr]}${open_meteo_hourly_winddirection_80m_output[$hr]}${open_meteo_hourly_winddirection_120m_output[$hr]}${open_meteo_hourly_winddirection_180m_output[$hr]}${open_meteo_hourly_windgusts_10m_output[$hr]}${open_meteo_hourly_soil_temperature_0cm_output[$hr]}${open_meteo_hourly_soil_temperature_6cm_output[$hr]}${open_meteo_hourly_soil_temperature_18cm_output[$hr]}${open_meteo_hourly_soil_temperature_54cm_output[$hr]}${open_meteo_hourly_soil_moisture_0_1cm_output[$hr]}${open_meteo_hourly_soil_moisture_1_3cm_output[$hr]}${open_meteo_hourly_soil_moisture_3_9cm_output[$hr]}${open_meteo_hourly_soil_moisture_9_27cm_output[$hr]}${open_meteo_hourly_soil_moisture_27_81cm_output[$hr]}"
            fi
        done
    }

    # Прогноз по дням Open-Meteo
    open_meteo_daily_weather() {
       open_meteo_daily_time_get() {
            open_meteo_daily_time_output+=(" | ${lng_str_date:-"Date:"} $(date -d @${open_meteo_daily_time[$d]:-0} +%d/%m)")
        }

        open_meteo_daily_weathercode_get() {
            open_meteo_weathercode=${open_meteo_daily_weathercode[$d]:-0}
            open_meteo_weather_code_converter
            open_meteo_daily_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $open_meteo_weathercode_text")
        }

        open_meteo_daily_temperature_2m_max_get() {
            open_meteo_daily_temperature_2m_max_output+=(" | ${lng_str_temperature_2m_max:-"Maximum Temperature (2 m):"} ${open_meteo_daily_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        open_meteo_daily_temperature_2m_min_get() {
            open_meteo_daily_temperature_2m_min_output+=(" | ${lng_str_temperature_2m_min:-"Minimum Temperature (2 m):"} ${open_meteo_daily_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        open_meteo_daily_apparent_temperature_2m_max_get() {
            open_meteo_daily_apparent_temperature_2m_max_output+=(" | ${lng_str_apparent_temperature_2m_max:-"Maximum Apparent Temperature (2 m):"} ${open_meteo_daily_apparent_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        open_meteo_daily_apparent_temperature_2m_min_get() {
            open_meteo_daily_apparent_temperature_2m_min_output+=(" | ${lng_str_apparent_temperature_2m_min:-"Minimum Apparent Temperature (2 m):"} ${open_meteo_daily_apparent_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        open_meteo_daily_sunrise_get() {
            open_meteo_daily_sunrise_output+=(" | ${lng_str_sunrise:-"Sunrise:"} $(date -d @${open_meteo_daily_sunrise[$d]:-0} +%H:%M)")
        }

        open_meteo_daily_sunset_get() {
            open_meteo_daily_sunset_output+=(" | ${lng_str_sunset:-"Sunset:"} $(date -d @${open_meteo_daily_sunset[$d]:-0} +%H:%M)")
        }

        open_meteo_daily_precipitation_sum_get() {
            open_meteo_daily_precipitation_sum_output+=(" | ${lng_str_precipitation_sum:-"Precipitation Sum:"} ${open_meteo_daily_precipitation_sum[$d]:-0} $precip_unit_text")
        }

        open_meteo_daily_rain_sum_get() {
            open_meteo_daily_rain_sum_output+=(" | ${lng_str_rain_sum:-"Rain Sum:"} ${open_meteo_daily_rain_sum[$d]:-0} $precip_unit_text")
        }

        open_meteo_daily_showers_sum_get() {
            open_meteo_daily_showers_sum_output+=(" | ${lng_str_showers_sum:-"Showers Sum:"} ${open_meteo_daily_showers_sum[$d]:-0} $precip_unit_text")
        }

        open_meteo_daily_snowfall_sum_get() {
            open_meteo_daily_snowfall_sum_output+=(" | ${lng_str_snowfall_sum:-"Snowfall Sum:"} ${open_meteo_daily_snowfall_sum[$d]:-0} $precip_unit_text")
        }

        open_meteo_daily_precipitation_hours_get() {
            open_meteo_daily_precipitation_hours_output+=(" | ${lng_str_precipitation_hours:-"Precipitation Hours:"} ${open_meteo_daily_precipitation_hours[$d]:-0} ${lng_str_unit_h:-"h"}")
        }

        open_meteo_daily_windspeed_10m_max_get() {
            open_meteo_daily_windspeed_10m_max_output+=(" | ${lng_str_windspeed_10m_max:-"Maximum Wind Speed (10 m):"} ${open_meteo_daily_windspeed_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        open_meteo_daily_windgusts_10m_max_get() {
            open_meteo_daily_windgusts_10m_max_output+=(" | ${lng_str_windgusts_10m_max:-"Maximum Wind Gust (10 m):"} ${open_meteo_daily_windgusts_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        open_meteo_daily_winddirection_10m_dominant_get() {
            wind_direction=$((${open_meteo_daily_winddirection_10m_dominant[$d]%.*}+1))
            wind_direction_convertor
            open_meteo_daily_winddirection_10m_dominant_output+=(" | ${lng_str_winddirection_10m_dominant:-"Dominant Wind Direction (10 m):"} $wind_direction_text")
        }

        open_meteo_daily_shortwave_radiation_sum_get() {
            open_meteo_daily_shortwave_radiation_sum_output+=(" | ${lng_str_shortwave_radiation_sum:-"Shortwave Radiation Sum:"} ${open_meteo_daily_shortwave_radiation_sum[$d]:-0} ${lng_str_unit_mjm2:-"MJ/m²"}")
        }

        open_meteo_daily_et0_fao_evapotranspiration_get() {
            open_meteo_daily_et0_fao_evapotranspiration_output+=(" | ${lng_str_et0_fao_evapotranspiration:-"Reference Evapotranspiration (ET₀):"} ${open_meteo_daily_et0_fao_evapotranspiration[$d]:-0} $precip_unit_text")
        }

        # Опции прогноза погоды по дням
        open_meteo_daily_options=($open_meteo_daily_time_enabled $open_meteo_daily_weathercode_enabled $open_meteo_daily_temperature_2m_max_enabled $open_meteo_daily_temperature_2m_min_enabled $open_meteo_daily_apparent_temperature_2m_max_enabled $open_meteo_daily_apparent_temperature_2m_min_enabled $open_meteo_daily_sunrise_enabled $open_meteo_daily_sunset_enabled $open_meteo_daily_precipitation_sum_enabled $open_meteo_daily_rain_sum_enabled $open_meteo_daily_showers_sum_enabled $open_meteo_daily_snowfall_sum_enabled $open_meteo_daily_precipitation_hours_enabled $open_meteo_daily_windspeed_10m_max_enabled $open_meteo_daily_windgusts_10m_max_enabled $open_meteo_daily_winddirection_10m_dominant_enabled $open_meteo_daily_shortwave_radiation_sum_enabled $open_meteo_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        open_meteo_daily_options_vallues=(open_meteo_daily_time_get open_meteo_daily_weathercode_get open_meteo_daily_temperature_2m_max_get open_meteo_daily_temperature_2m_min_get open_meteo_daily_apparent_temperature_2m_max_get open_meteo_daily_apparent_temperature_2m_min_get open_meteo_daily_sunrise_get open_meteo_daily_sunset_get open_meteo_daily_precipitation_sum_get open_meteo_daily_rain_sum_get open_meteo_daily_showers_sum_get open_meteo_daily_snowfall_sum_get open_meteo_daily_precipitation_hours_get open_meteo_daily_windspeed_10m_max_get open_meteo_daily_windgusts_10m_max_get open_meteo_daily_winddirection_10m_dominant_get open_meteo_daily_shortwave_radiation_sum_get open_meteo_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!open_meteo_daily_options[*]}
        do
            if [[ ${open_meteo_daily_options[$om_do]} == "true" ]]
            then
                open_meteo_daily_options_array+=(${open_meteo_daily_options_vallues[$om_do]})
            fi
        done

        for open_meteo_daily_options_array_n in ${!open_meteo_daily_options_array[*]}
        do
            for ((d=0;d<$open_meteo_days;d++))
            do
                ${open_meteo_daily_options_array[$open_meteo_daily_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по дням
        for ((day=0;day<$open_meteo_days;day++))
        do
            echo "|#| Open-Meteo${open_meteo_daily_time_output[$day]}${open_meteo_daily_weathercode_output[$day]}${open_meteo_daily_temperature_2m_max_output[$day]}${open_meteo_daily_temperature_2m_min_output[$day]}${open_meteo_daily_apparent_temperature_2m_max_output[$day]}${open_meteo_daily_apparent_temperature_2m_min_output[$day]}${open_meteo_daily_sunrise_output[$day]}${open_meteo_daily_sunset_output[$day]}${open_meteo_daily_precipitation_sum_output[$day]}${open_meteo_daily_rain_sum_output[$day]}${open_meteo_daily_showers_sum_output[$day]}${open_meteo_daily_snowfall_sum_output[$day]}${open_meteo_daily_precipitation_hours_output[$day]}${open_meteo_daily_windspeed_10m_max_output[$day]}${open_meteo_daily_windgusts_10m_max_output[$day]}${open_meteo_daily_winddirection_10m_dominant_output[$day]}${open_meteo_daily_shortwave_radiation_sum_output[$day]}${open_meteo_daily_et0_fao_evapotranspiration_output[$day]}"
        done
    }

    units

    # Проверка включённых параметров Open-Meteo
    open_meteo_array_1=($open_meteo_current_weather_enabled $open_meteo_daily_weather_enabled $open_meteo_hourly_weather_enabled)
    open_meteo_array_2=(open_meteo_current_weather open_meteo_daily_weather open_meteo_hourly_weather)

    for om_array_x in ${!open_meteo_array_1[*]}
    do
        if [[ ${open_meteo_array_1[$om_array_x]} == "true" ]]
        then
            ${open_meteo_array_2[$om_array_x]}
        fi
    done
}

if [[ $open_meteo_current_weather_enabled == "true" || $open_meteo_daily_weather_enabled == "true" || $open_meteo_hourly_weather_enabled == "true" ]]
then
    open_meteo
fi

# MI провайдер
met_no() {
    # Конвертер числового значения погоды в текстовое
    met_no_weather_code_converter() {
        # Объявление массивов для числовых значений погоды
        met_no_weathercode_symbol=('clearsky' 'cloudy' 'fair' 'fog' 'heavyrain' 'heavyrainandthunder' 'heavyrainshowers' 'heavyrainshowersandthunder' 'heavysleet' 'heavysleetandthunder' 'heavysleetshowers' 'heavysleetshowersandthunder' 'heavysnow' 'heavysnowandthunder' 'heavysnowshowers' 'heavysnowshowersandthunder' 'lightrain' 'lightrainandthunder' 'lightrainshowers' 'lightrainshowersandthunder' 'lightsleet' 'lightsleetandthunder' 'lightsleetshowers' 'lightsnow' 'lightsnowandthunder' 'lightsnowshowers' 'lightssleetshowersandthunder' 'lightssnowshowersandthunder' 'partlycloudy' 'rain' 'rainandthunder' 'rainshowers' 'rainshowersandthunder' 'sleet' 'sleetandthunder' 'sleetshowers' 'sleetshowersandthunder' 'snow' 'snowandthunder' 'snowshowers' 'snowshowersandthunder')

        # Объявление массивов для текстовых значений погоды
        met_no_weathercode_text_array=("${lng_str_clear_sky:-"Clear sky"}" "${lng_str_cloudy:-"Cloudy"}" "${lng_str_fair:-"Fair"}" "${lng_str_fog:-"Fog"}" "${lng_str_heavyrain:-"Heavy rain"}" "${lng_str_heavyrainandthunder:-"Heavy rain and thunder"}" "${lng_str_heavyrainshowers:-"Heavy rain showers"}" "${lng_str_heavyrainshowersandthunder:-"Heavy rain showers and thunder"}" "${lng_str_heavysleet:-"Heavy sleet"}" "${lng_str_heavysleetandthunder:-"Heavy sleet and thunder"}" "${lng_str_heavysleetshowers:-"Heavy sleet showers"}" "${lng_str_heavysleetshowersandthunder:-"Heavy sleet showers and thunder"}" "${lng_str_heavysnow:-"Heavy snow"}" "${lng_str_heavysnowandthunder:-"Heavy snow and thunder"}" "${lng_str_heavysnowshowers:-"Heavy snow showers"}" "${lng_str_heavysnowshowersandthunder:-"Heavy snow showers and thunder"}" "${lng_str_lightrain:-"Light rain"}" "${lng_str_lightrainandthunder:-"Light rain and thunder"}" "${lng_str_lightrainshowers:-"Light rain showers"}" "${lng_str_lightrainshowersandthunder:-"Light rain showers and thunder"}" "${lng_str_lightsleet:-"Light sleet"}" "${lng_str_lightsleetandthunder:-"Light sleet and thunder"}" "${lng_str_lightsleetshowers:-"Light sleet showers"}" "${lng_str_lightsnow:-"Light snow"}" "${lng_str_lightsnowandthunder:-"Light snow and thunder"}" "${lng_str_lightsnowshowers:-"Light snow showers"}" "${lng_str_lightsleetshowersandthunder:-"Light sleet showers and thunder"}" "${lng_str_lightsnowshowersandthunder:-"Light snow showers and thunder"}" "${lng_str_partlycloudy:-"Partly cloudy"}" "${lng_str_rain:-"Rain"}" "${lng_str_rainandthunder:-"Rain and thunder"}" "${lng_str_rainshowers:-"Rain showers"}" "${lng_str_rainshowersandthunder:-"Rain showers and thunder"}" "${lng_str_sleet:-"Sleet"}" "${lng_str_sleetandthunder:-"Sleet and thunder"}" "${lng_str_sleetshowers:-"Sleet showers"}" "${lng_str_sleetshowersandthunder:-"Sleet showers and thunder"}" "${lng_str_snow:-"Snow"}" "${lng_str_snowandthunder:-"Snow and thunder"}" "${lng_str_snowshowers:-"Snow showers"}" "${lng_str_snowshowersandthunder:-"Snow showers and thunder"}")

        # Конвертация числового кода погоды в текстовый
        for met_no_wc in ${!met_no_weathercode_symbol[*]}
        do
            if [[ $met_no_weathercode == ${met_no_weathercode_symbol[$met_no_wc]} ]]
            then
                met_no_weathercode_text=${met_no_weathercode_text_array[$met_no_wc]}
            fi
        done
    }

    # Текущая погода MI
    met_no_current_weather() {
        met_no_current_time_get() {
            met_no_current_time_output=" | ${lng_str_time:-"Time:"} $(date -d ${met_no_current_weather_array[0]:-0})"
        }

        met_no_current_temperature_get() {
            met_no_current_temperature_output=" | ${lng_str_temperature:-"Temperature:"} ${met_no_current_weather_array[2]:-0} $temp_unit_text"
        }

        met_no_current_weathercode_get() {
            # Конвертация значения текущей погоды
            met_no_weathercode=$(echo ${met_no_current_weather_array[13]:-0} | sed -r 's/_.+//')
            met_no_weather_code_converter
            met_no_current_weathercode_output=" | ${lng_str_weather:-"Weather:"} $met_no_weathercode_text"
        }

        met_no_current_wind_direction_get() {
            wind_direction_raw=${met_no_current_weather_array[11]:-0}
            # Округление значения направления ветра
            wind_direction=${wind_direction_raw%.*}
            wind_direction_convertor
            met_no_current_wind_direction_output=" | ${lng_str_wind_direction:-"Wind Direction:"} $wind_direction_text"
        }

        met_no_current_wind_speed_get() {
            met_no_current_wind_speed_output=" | ${lng_str_wind_speed:-"Wind Speed:"} ${met_no_current_weather_array[12]:-0} ${lng_str_wind_unit_ms:-"m/s"}"
        }

        # Включенные опции для почасового прогноза погоды
        met_no_current_options=($met_no_current_time_enabled $met_no_current_temperature_enabled $met_no_current_weathercode_enabled $met_no_current_wind_direction_enabled $met_no_current_wind_speed_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        met_no_current_options_vallues=(met_no_current_time_get met_no_current_temperature_get met_no_current_weathercode_get met_no_current_wind_direction_get met_no_current_wind_speed_get)

        for met_no_current in ${!met_no_current_options[*]}
        do
            if [[ ${met_no_current_options[$met_no_current]} == "true" ]]
            then
                met_no_current_options_array+=(${met_no_current_options_vallues[$met_no_current]})
            fi
        done

        # Обработка включенных опций
        for met_no_current_options_array_n in ${!met_no_current_options_array[*]}
        do
            ${met_no_current_options_array[$met_no_current_options_array_n]}
        done

        # Вывод текущей погоды
        echo "|#| MET.NO | ${lng_str_current_weather:-"Current weather"}$met_no_current_temperature_output$met_no_current_wind_speed_output$met_no_current_wind_direction_output$met_no_current_weathercode_output$met_no_current_time_output"
    }

    # Прогноз по часам MI
    met_no_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        met_no_hourly_time_get() {
            met_no_hourly_time_output+=(" | ${lng_str_time:-"Time:"} $(date -d ${met_no_hourly_time[$h]:-0} +"%d/%m %H:%M")")
        }

        met_no_hourly_air_pressure_at_sea_level_get() {
            met_no_hourly_air_pressure_at_sea_level_output+=(" | ${lng_str_pressure_msl:-"Sealevel Pressure:"} ${met_no_hourly_air_pressure_at_sea_level[$h]:-0} ${lng_str_pressure_unit_hpa:-"hPa"}")
        }

        met_no_hourly_air_temperature_get() {
            met_no_hourly_air_temperature_output+=(" | ${lng_str_temperature:-"Temperature:"} ${met_no_hourly_air_temperature[$h]:-0} $temp_unit_text")
        }

        met_no_hourly_cloud_area_fraction_get() {
            met_no_hourly_cloud_area_fraction_output+=(" | ${lng_str_cloudcover_total:-"Cloud Cover (total):"} ${met_no_hourly_cloud_area_fraction[$h]:-0} %")
        }

        met_no_hourly_cloud_area_fraction_low_get() {
            met_no_hourly_cloud_area_fraction_low_output+=(" | ${lng_str_cloudcover_low:-"Cloud Cover (low):"} $met_no_hourly_cloud_area_fraction_low %")
        }

        met_no_hourly_cloud_area_fraction_medium_get() {
            met_no_hourly_cloud_area_fraction_medium_output+=(" | ${lng_str_cloudcover_mid:-"Cloud Cover (mid):"} ${met_no_hourly_cloud_area_fraction_medium[$h]:-0} %")
        }

        met_no_hourly_cloud_area_fraction_high_get() {
            met_no_hourly_cloud_area_fraction_high_output+=(" | ${lng_str_cloudcover_high:-"Cloud Cover (high):"} ${met_no_hourly_cloud_area_fraction_high[$h]:-0} %")
        }

        met_no_hourly_dew_point_temperature_get() {
            met_no_hourly_dew_point_temperature_output+=(" | ${lng_str_dewpoint:-"Dewpoint:"} ${met_no_hourly_dew_point_temperature[$h]:-0} $temp_unit_text")
        }

        met_no_hourly_relative_humidity_get() {
            met_no_hourly_relative_humidity_output+=(" | ${lng_str_relative_humidity:-"Relative Humidity:"} ${met_no_hourly_relative_humidity[$h]:-0} %")
        }

        met_no_hourly_weathercode_get() {
            met_no_weathercode=$(echo ${met_no_hourly_weathercode[$h]:-0} | sed -r 's/_.+//')
            met_no_weather_code_converter
            met_no_hourly_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $met_no_weathercode_text")
        }

        met_no_hourly_wind_speed_get() {
            met_no_hourly_wind_speed_output+=(" | ${lng_str_wind_speed:-"Wind Speed:"} ${met_no_hourly_wind_speed[$h]:-0} $wind_unit_text")
        }

        met_no_hourly_wind_from_direction_get() {
            wind_direction_raw=${met_no_hourly_wind_from_direction[$h]:-0}
            wind_direction=${wind_direction_raw%.*}
            wind_direction_convertor
            met_no_hourly_wind_from_direction_output+=(" | ${lng_str_wind_direction:-"Wind Direction:"} $wind_direction_text")
        }


        # Включенные опции для почасового прогноза погоды
        met_no_hourly_options=($met_no_hourly_time_enabled $met_no_hourly_air_pressure_at_sea_level_enabled $met_no_hourly_air_temperature_enabled $met_no_hourly_cloud_area_fraction_enabled $met_no_hourly_cloud_area_fraction_low_enabled $met_no_hourly_cloud_area_fraction_medium_enabled $met_no_hourly_cloud_area_fraction_high_enabled $met_no_hourly_dew_point_temperature_enabled $met_no_hourly_fog_area_fraction_enabled $met_no_hourly_precipitation_amount_enabled $met_no_hourly_relative_humidity_enabled $met_no_hourly_ultraviolet_index_clear_sky_enabled $met_no_hourly_wind_from_direction_enabled $met_no_hourly_wind_speed_enabled $met_no_hourly_weathercode_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        met_no_hourly_options_vallues=(met_no_hourly_time_get met_no_hourly_air_pressure_at_sea_level_get met_no_hourly_air_temperature_get met_no_hourly_cloud_area_fraction_get met_no_hourly_cloud_area_fraction_low_get met_no_hourly_cloud_area_fraction_medium_get met_no_hourly_cloud_area_fraction_high_get met_no_hourly_dew_point_temperature_get met_no_hourly_fog_area_fraction_get met_no_hourly_precipitation_amount_get met_no_hourly_relative_humidity_get met_no_hourly_ultraviolet_index_clear_sky_get met_no_hourly_wind_from_direction_get met_no_hourly_wind_speed_get met_no_hourly_weathercode_get)

        for om_ho in ${!met_no_hourly_options[*]}
        do
            if [[ ${met_no_hourly_options[$om_ho]} == "true" ]]
            then
                met_no_hourly_options_array+=(${met_no_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for met_no_hourly_options_array_n in ${!met_no_hourly_options_array[*]}
        do
            for ((h=0;h<$met_no_hours;h++))
            do
                ${met_no_hourly_options_array[$met_no_hourly_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по часам
        for ((hr=0;hr<$met_no_hours;hr++))
        do
            if (( $(date +%s -d ${met_no_hourly_time[$hr]}) > $(date +%s) ))
            then
                echo "|#| MET.NO${met_no_hourly_time_output[$hr]}${met_no_hourly_air_pressure_at_sea_level_output[$hr]}${met_no_hourly_air_temperature_output[$hr]}${met_no_hourly_cloud_area_fraction_output[$hr]}${met_no_hourly_cloud_area_fraction_low_output[$hr]}${met_no_hourly_cloud_area_fraction_medium_output[$hr]}${met_no_hourly_cloud_area_fraction_high_output[$hr]}${met_no_hourly_dew_point_temperature_output[$hr]}${met_no_hourly_fog_area_fraction_output[$hr]}${met_no_hourly_precipitation_amount_output[$hr]}${met_no_hourly_relative_humidity_output[$hr]}${met_no_hourly_ultraviolet_index_clear_sky_output[$hr]}${met_no_hourly_wind_from_direction_output[$hr]}${met_no_hourly_wind_speed_output[$hr]}${met_no_hourly_weathercode_output[$hr]}"
            fi
        done
    }

    # Прогноз по дням MI
    met_no_daily_weather() {
       met_no_daily_time_get() {
            met_no_daily_time_output+=(" | ${lng_str_date:-"Date:"} $(date -d @${met_no_daily_time[$d]:-0} +%d/%m)")
        }

        met_no_daily_weathercode_get() {
            met_no_weathercode=${met_no_daily_weathercode[$d]:-0}
            met_no_weather_code_converter
            met_no_daily_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $met_no_weathercode_text")
        }

        met_no_daily_temperature_2m_max_get() {
            met_no_daily_temperature_2m_max_output+=(" | ${lng_str_temperature_2m_max:-"Maximum Temperature (2 m):"} ${met_no_daily_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        met_no_daily_temperature_2m_min_get() {
            met_no_daily_temperature_2m_min_output+=(" | ${lng_str_temperature_2m_min:-"Minimum Temperature (2 m):"} ${met_no_daily_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        met_no_daily_apparent_temperature_2m_max_get() {
            met_no_daily_apparent_temperature_2m_max_output+=(" | ${lng_str_apparent_temperature_2m_max:-"Maximum Apparent Temperature (2 m):"} ${met_no_daily_apparent_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        met_no_daily_apparent_temperature_2m_min_get() {
            met_no_daily_apparent_temperature_2m_min_output+=(" | ${lng_str_apparent_temperature_2m_min:-"Minimum Apparent Temperature (2 m):"} ${met_no_daily_apparent_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        met_no_daily_sunrise_get() {
            met_no_daily_sunrise_output+=(" | ${lng_str_sunrise:-"Sunrise:"} $(date -d @${met_no_daily_sunrise[$d]:-0} +%H:%M)")
        }

        met_no_daily_sunset_get() {
            met_no_daily_sunset_output+=(" | ${lng_str_sunset:-"Sunset:"} $(date -d @${met_no_daily_sunset[$d]:-0} +%H:%M)")
        }

        met_no_daily_precipitation_sum_get() {
            met_no_daily_precipitation_sum_output+=(" | ${lng_str_precipitation_sum:-"Precipitation Sum:"} ${met_no_daily_precipitation_sum[$d]:-0} $precip_unit_text")
        }

        met_no_daily_rain_sum_get() {
            met_no_daily_rain_sum_output+=(" | ${lng_str_rain_sum:-"Rain Sum:"} ${met_no_daily_rain_sum[$d]:-0} $precip_unit_text")
        }

        met_no_daily_showers_sum_get() {
            met_no_daily_showers_sum_output+=(" | ${lng_str_showers_sum:-"Showers Sum:"} ${met_no_daily_showers_sum[$d]:-0} $precip_unit_text")
        }

        met_no_daily_snowfall_sum_get() {
            met_no_daily_snowfall_sum_output+=(" | ${lng_str_snowfall_sum:-"Snowfall Sum:"} ${met_no_daily_snowfall_sum[$d]:-0} $precip_unit_text")
        }

        met_no_daily_precipitation_hours_get() {
            met_no_daily_precipitation_hours_output+=(" | ${lng_str_precipitation_hours:-"Precipitation Hours:"} ${met_no_daily_precipitation_hours[$d]:-0} ${lng_str_unit_h:-"h"}")
        }

        met_no_daily_windspeed_10m_max_get() {
            met_no_daily_windspeed_10m_max_output+=(" | ${lng_str_windspeed_10m_max:-"Maximum Wind Speed (10 m):"} ${met_no_daily_windspeed_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        met_no_daily_windgusts_10m_max_get() {
            met_no_daily_windgusts_10m_max_output+=(" | ${lng_str_windgusts_10m_max:-"Maximum Wind Gust (10 m):"} ${met_no_daily_windgusts_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        met_no_daily_winddirection_10m_dominant_get() {
            wind_direction=$((${met_no_daily_winddirection_10m_dominant[$d]%.*}+1))
            wind_direction_convertor
            met_no_daily_winddirection_10m_dominant_output+=(" | ${lng_str_winddirection_10m_dominant:-"Dominant Wind Direction (10 m):"} $wind_direction_text")
        }

        met_no_daily_shortwave_radiation_sum_get() {
            met_no_daily_shortwave_radiation_sum_output+=(" | ${lng_str_shortwave_radiation_sum:-"Shortwave Radiation Sum:"} ${met_no_daily_shortwave_radiation_sum[$d]:-0} ${lng_str_unit_mjm2:-"MJ/m²"}")
        }

        met_no_daily_et0_fao_evapotranspiration_get() {
            met_no_daily_et0_fao_evapotranspiration_output+=(" | ${lng_str_et0_fao_evapotranspiration:-"Reference Evapotranspiration (ET₀):"} ${met_no_daily_et0_fao_evapotranspiration[$d]:-0} $precip_unit_text")
        }

        # Опции прогноза погоды по дням
        met_no_daily_options=($met_no_daily_time_enabled $met_no_daily_weathercode_enabled $met_no_daily_temperature_2m_max_enabled $met_no_daily_temperature_2m_min_enabled $met_no_daily_apparent_temperature_2m_max_enabled $met_no_daily_apparent_temperature_2m_min_enabled $met_no_daily_sunrise_enabled $met_no_daily_sunset_enabled $met_no_daily_precipitation_sum_enabled $met_no_daily_rain_sum_enabled $met_no_daily_showers_sum_enabled $met_no_daily_snowfall_sum_enabled $met_no_daily_precipitation_hours_enabled $met_no_daily_windspeed_10m_max_enabled $met_no_daily_windgusts_10m_max_enabled $met_no_daily_winddirection_10m_dominant_enabled $met_no_daily_shortwave_radiation_sum_enabled $met_no_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        met_no_daily_options_vallues=(met_no_daily_time_get met_no_daily_weathercode_get met_no_daily_temperature_2m_max_get met_no_daily_temperature_2m_min_get met_no_daily_apparent_temperature_2m_max_get met_no_daily_apparent_temperature_2m_min_get met_no_daily_sunrise_get met_no_daily_sunset_get met_no_daily_precipitation_sum_get met_no_daily_rain_sum_get met_no_daily_showers_sum_get met_no_daily_snowfall_sum_get met_no_daily_precipitation_hours_get met_no_daily_windspeed_10m_max_get met_no_daily_windgusts_10m_max_get met_no_daily_winddirection_10m_dominant_get met_no_daily_shortwave_radiation_sum_get met_no_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!met_no_daily_options[*]}
        do
            if [[ ${met_no_daily_options[$om_do]} == "true" ]]
            then
                met_no_daily_options_array+=(${met_no_daily_options_vallues[$om_do]})
            fi
        done

        for met_no_daily_options_array_n in ${!met_no_daily_options_array[*]}
        do
            for ((d=0;d<$met_no_days;d++))
            do
                ${met_no_daily_options_array[$met_no_daily_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по дням
        for ((day=0;day<$met_no_days;day++))
        do
            echo "|#| MET.NO${met_no_daily_time_output[$day]}${met_no_daily_weathercode_output[$day]}${met_no_daily_temperature_2m_max_output[$day]}${met_no_daily_temperature_2m_min_output[$day]}${met_no_daily_apparent_temperature_2m_max_output[$day]}${met_no_daily_apparent_temperature_2m_min_output[$day]}${met_no_daily_sunrise_output[$day]}${met_no_daily_sunset_output[$day]}${met_no_daily_precipitation_sum_output[$day]}${met_no_daily_rain_sum_output[$day]}${met_no_daily_showers_sum_output[$day]}${met_no_daily_snowfall_sum_output[$day]}${met_no_daily_precipitation_hours_output[$day]}${met_no_daily_windspeed_10m_max_output[$day]}${met_no_daily_windgusts_10m_max_output[$day]}${met_no_daily_winddirection_10m_dominant_output[$day]}${met_no_daily_shortwave_radiation_sum_output[$day]}${met_no_daily_et0_fao_evapotranspiration_output[$day]}"
        done
    }

    units

    # Проверка включённых параметров MI
    met_no_array_1=($met_no_current_weather_enabled $met_no_daily_weather_enabled $met_no_hourly_weather_enabled)
    met_no_array_2=(met_no_current_weather met_no_daily_weather met_no_hourly_weather)

    for met_no_array_x in ${!met_no_array_1[*]}
    do
        if [[ ${met_no_array_1[$met_no_array_x]} == "true" ]]
        then
            ${met_no_array_2[$met_no_array_x]}
        fi
    done
}

if [[ $met_no_current_weather_enabled == "true" || $met_no_daily_weather_enabled == "true" || $met_no_hourly_weather_enabled == "true" ]]
then
    met_no
fi

# OpenWeatherMap провайдер
owm() {
    # Конвертер числового значения погоды в текстовое
    owm_weather_code_converter() {
        # Объявление массивов для числовых значений погоды
        owm_weathercode_number=(200 201 202 210 211 212 221 230 231 232 300 301 302 310 311 312 313 314 321 500 501 502 503 504 511 520 521 522 531 600 601 602 611 612 613 615 616 620 621 622 701 711 721 731 741 751 761 762 771 781 800 801 802 803 804)

        # Объявление массивов для текстовых значений погоды
        owm_weathercode_text_array=("${lng_str_thunderstorm_with_light_rain:-"Thunderstorm with light rain"}" "${lng_str_thunderstorm_with_rain:-"Thunderstorm with rain"}" "${lng_str_thunderstorm_with_heavy_rain:-"Thunderstorm with heavy rain"}" "${lng_str_light_thunderstorm:-"Light thunderstorm"}" "${lng_str_thunderstorm:-"Thunderstorm"}" "${lng_str_heavy_thunderstorm:-"Heavy thunderstorm"}" "${lng_str_ragged_thunderstorm:-"Ragged thunderstorm"}" "${lng_str_thunderstorm_with_light_drizzle:-"Thunderstorm with light drizzle"}" "${lng_str_thunderstorm_with_drizzle:-"Thunderstorm with drizzle"}" "${lng_str_thunderstorm_with_heavy_drizzle:-"Thunderstorm with heavy drizzle"}" "${lng_str_light_intensity_drizzle:-"Light drizzle"}" "${lng_str_drizzle:-"Drizzle"}" "${lng_str_heavy_intensity_drizzle:-"Heavy drizzle"}" "${lng_str_light_intensity_drizzle_rain:-"Light drizzle rain"}" "${lng_str_drizzle_rain:-"Drizzle rain"}" "${lng_str_heavy_intensity_drizzle_rain:-"Heavy drizzle rain"}" "${lng_str_shower_rain_and_drizzle:-"Shower rain and drizzle"}" "${lng_str_heavy_shower_rain_and_drizzle:-"Heavy shower rain and drizzle"}" "${lng_str_shower_drizzle:-"Shower drizzle"}" "${lng_str_light_rain:-"Light rain"}" "${lng_str_moderate_rain:-"Moderate rain"}" "${lng_str_heavy_intensity_rain:-"Heavy rain"}" "${lng_str_very_heavy_rain:-"Very heavy rain"}" "${lng_str_extreme_rain:-"Extreme rain"}" "${lng_str_freezing_rain:-"Freezing rain"}" "${lng_str_light_intensity_shower_rain:-"Light shower rain"}" "${lng_str_shower_rain:-"Shower rain"}" "${lng_str_heavy_intensity_shower_rain:-"Heavy shower rain"}" "${lng_str_ragged_shower_rain:-"Ragged shower rain"}" "${lng_str_light_snow:-"Light snow"}" "${lng_str_snow:-"Snow"}" "${lng_str_heavy_snow:-"Heavy snow"}" "${lng_str_sleet:-"Sleet"}" "${lng_str_light_shower_sleet:-"Light shower sleet"}" "${lng_str_shower_sleet:-"Shower sleet"}" "${lng_str_light_rain_and_snow:-"Light rain and snow"}" "${lng_str_rain_and_snow:-"Rain and snow"}" "${lng_str_light_shower_snow:-"Light shower snow"}" "${lng_str_shower_snow:-"Shower snow"}" "${lng_str_heavy_shower_snow:-"Heavy shower snow"}" "${lng_str_mist:-"Mist"}" "${lng_str_smoke:-"Smoke"}" "${lng_str_haze:-"Haze"}" "${lng_str_dust_whirls:-"Dust whirls"}" "${lng_str_fog:-"Fog"}" "${lng_str_sand:-"Sand"}" "${lng_str_dust:-"Dust"}" "${lng_str_volcanic_ash:-"Volcanic Ash"}" "${lng_str_squalls:-"Squalls"}" "${lng_str_tornado:-"Tornado"}" "${lng_str_clear_sky:-"Clear sky"}" "${lng_str_few_clouds:-"Few clouds"}" "${lng_str_scattered_clouds:-"Scattered clouds"}" "${lng_str_broken_clouds:-"Broken clouds"}" "${lng_str_overcast_clouds:-"Overcast clouds"}")

        # Конвертация числового кода погоды в текстовый
        for owm_wc in ${!owm_weathercode_number[*]}
        do
            if [[ $owm_weathercode == ${owm_weathercode_number[$owm_wc]} ]]
            then
                owm_weathercode_text=${owm_weathercode_text_array[$owm_wc]}
            fi
        done
    }

    # Текущая погода OpenWeatherMap
    owm_current_weather() {
        owm_current_time_get() {
            owm_current_time_output=" | ${lng_str_time:-"Time:"} $(date -d @${owm_current_weather[0]:-0})"
        }

        owm_current_sunrise_get() {
            owm_current_sunrise_output=" | ${lng_str_sunrise:-"Sunrise:"} $(date -d @${owm_current_weather[1]:-0} +%H:%M)"
        }

        owm_current_sunset_get() {
            owm_current_sunset_output=" | ${lng_str_sunset:-"Sunset:"} $(date -d @${owm_current_weather[2]:-0} +%H:%M)"
        }

        owm_current_temp_get() {
            owm_current_temp_output=" | ${lng_str_temperature:-"Temperature:"} ${owm_current_weather[3]:-0} $temp_unit_text"
        }

        owm_current_temp_min_get() {
            owm_current_temp_min_output=" | ${lng_str_temperature_min:-"Min temperature:"} ${owm_current_weather[4]:-0} $temp_unit_text"
        }

        owm_current_temp_max_get() {
            owm_current_temp_max_output=" | ${lng_str_temperature_max:-"Max temperature:"} ${owm_current_weather[5]:-0} $temp_unit_text"
        }

        owm_current_feels_like_get() {
            owm_current_feels_like_output=" | ${lng_str_feels_like:-"Feels like:"} ${owm_current_weather[6]:-0} $temp_unit_text"
        }

        owm_current_pressure_get() {
            owm_current_pressure_output=" | ${lng_str_pressure:-"Pressure:"} ${owm_current_weather[7]:-0} $pressure_unit_text"
        }

        owm_current_humidity_get() {
            owm_current_humidity_output=" | ${lng_str_humidity:-"Humidity:"} ${owm_current_weather[8]:-0} %"
        }

        owm_current_clouds_get() {
            owm_current_clouds_output=" | ${lng_str_clouds:-"Cloudiness::"} ${owm_current_weather[9]:-0} %"
        }

        owm_current_visibility_get() {
            owm_current_visibility_output=" | ${lng_str_visibility:-"Visibility:"} ${owm_current_weather[10]:-0} ${lng_str_unit_m:-"m"}"
        }

        owm_current_wind_speed_get() {
            owm_current_wind_speed_output=" | ${lng_str_wind_speed:-"Wind Speed:"} ${owm_current_weather[11]:-0} $wind_unit_text"
        }

        owm_current_wind_deg_get() {
            wind_direction=${owm_current_weather[12]:-0}
            wind_direction_convertor
            owm_current_wind_deg_output=" | ${lng_str_wind_direction:-"Wind Direction:"} $wind_direction_text"
        }

        owm_current_wind_gust_get() {
            owm_current_wind_gust_output=" | ${lng_str_windgusts:-"Wind Gusts:"} ${owm_current_weather[13]:-0} $wind_unit_text"
        }

        owm_current_weathercode_get() {
            # Конвертация значения текущей погоды
            owm_weathercode=${owm_current_weather[14]:-0}
            owm_weather_code_converter
            owm_current_weathercode_output=" | ${lng_str_weather:-"Weather:"} $owm_weathercode_text"
        }

        # Включенные опции для почасового прогноза погоды
        owm_current_options=($owm_current_time_enabled $owm_current_sunrise_enabled $owm_current_sunset_enabled $owm_current_temp_enabled $owm_current_temp_min_enabled $owm_current_temp_max_enabled $owm_current_feels_like_enabled $owm_current_pressure_enabled $owm_current_humidity_enabled $owm_current_clouds_enabled $owm_current_visibility_enabled $owm_current_wind_speed_enabled $owm_current_wind_deg_enabled $owm_current_wind_gust_enabled $owm_current_weathercode_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        owm_current_options_vallues=(owm_current_time_get owm_current_sunrise_get owm_current_sunset_get owm_current_temp_get owm_current_temp_min_get owm_current_temp_max_get owm_current_feels_like_get owm_current_pressure_get owm_current_humidity_get owm_current_clouds_get owm_current_visibility_get owm_current_wind_speed_get owm_current_wind_deg_get owm_current_wind_gust_get owm_current_weathercode_get)

        for owm_current in ${!owm_current_options[*]}
        do
            if [[ ${owm_current_options[$owm_current]} == "true" ]]
            then
                owm_current_options_array+=(${owm_current_options_vallues[$owm_current]})
            fi
        done

        # Обработка включенных опций
        for owm_current_options_array_n in ${!owm_current_options_array[*]}
        do
            ${owm_current_options_array[$owm_current_options_array_n]}
        done

        # Вывод текущей погоды
        echo "|#| OpenWeatherMap | ${lng_str_current_weather:-"Current weather"}$owm_current_temp_output$owm_current_temp_min_output$owm_current_temp_max_output$owm_current_feels_like_output$owm_current_pressure_output$owm_current_humidity_output$owm_current_dew_point_output$owm_current_uvi_output$owm_current_clouds_output$owm_current_visibility_output$owm_current_wind_speed_output$owm_current_wind_deg_output$owm_current_wind_gust_output$owm_current_weathercode_output$owm_current_sunrise_output$owm_current_sunset_output$owm_current_time_output"
    }

    # Прогноз по часам OpenWeatherMap
    owm_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        owm_hourly_time_get() {
            owm_hourly_time_output+=(" | ${lng_str_time:-"Time:"} $(date -d @${owm_hourly_time[$h]:-0} +"%d/%m %H:%M")")
        }

        owm_hourly_temperature_2m_get() {
            owm_hourly_temperature_2m_output+=(" | ${lng_str_temperature_2m:-"Temperature (2m):"} ${owm_hourly_temperature_2m[$h]:-0} $temp_unit_text")
        }

        owm_hourly_relativehumidity_2m_get() {
            owm_hourly_relativehumidity_2m_output+=(" | ${lng_str_relativehumidity_2m:-"Relative Humidity (2m):"} ${owm_hourly_relativehumidity_2m[$h]:-0} %")
        }

        owm_hourly_dewpoint_2m_get() {
            owm_hourly_dewpoint_2m_output+=(" | ${lng_str_dewpoint_2m:-"Dewpoint (2m):"} ${owm_hourly_dewpoint_2m[$h]:-0} $temp_unit_text")
        }

        owm_hourly_apparent_temperature_get() {
            owm_hourly_apparent_temperature_output+=(" | ${lng_str_apparent_temperature:-"Apparent Temperature:"} ${owm_hourly_apparent_temperature[$h]:-0} $temp_unit_text")
        }

        owm_hourly_pressure_msl_get() {
            owm_hourly_pressure_msl_output+=(" | ${lng_str_pressure_msl:-"Sealevel Pressure:"} ${owm_hourly_pressure_msl[$h]:-0} ${lng_str_pressure_unit_hpa:-"hPa"}")
        }

        owm_hourly_surface_pressure_get() {
            owm_hourly_surface_pressure_output+=(" | ${lng_str_surface_pressure:-"Surface Pressure:"} ${owm_hourly_surface_pressure[$h]:-0} ${lng_str_pressure_unit_hpa:-"hPa"}")
        }

        owm_hourly_precipitation_get() {
            owm_hourly_precipitation_output+=(" | ${lng_str_precipitation_rain_showers_snow:-"Precipitation (rain + showers + snow):"} ${owm_hourly_precipitation[$h]:-0} $precip_unit_text")
        }

        owm_hourly_rain_get() {
            owm_hourly_rain_output+=(" | ${lng_str_rain_text:-"Rain:"} ${owm_hourly_rain[$h]:-0} $precip_unit_text")
        }

        owm_hourly_showers_get() {
            owm_hourly_showers_output+=(" | ${lng_str_showers:-"Showers:"} ${owm_hourly_showers[$h]:-0} $precip_unit_text")
        }

        owm_hourly_snowfall_get() {
            owm_hourly_snowfall_output+=(" | ${lng_str_snowfall:-"Snowfall:"} ${owm_hourly_snowfall[$h]:-0} ${lng_str_unit_cm:-"cm"}")
        }

        owm_hourly_weathercode_get() {
            owm_weathercode=${owm_hourly_weathercode[$h]:-0}
            owm_weather_code_converter
            owm_hourly_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $owm_weathercode_text")
        }

        owm_hourly_snow_depth_get() {
            owm_hourly_snow_depth_output+=(" | ${lng_str_snow_depth:-"Snow Depth:"} ${owm_hourly_snow_depth[$h]:-0} ${lng_str_unit_m:-"m"}")
        }

        owm_hourly_freezinglevel_height_get() {
            owm_hourly_freezinglevel_height_output+=(" | ${lng_str_freezinglevel_height:-"Freezinglevel Height:"} ${owm_hourly_freezinglevel_height[$h]:-0} ${lng_str_unit_m:-"m"}")
        }

        owm_hourly_cloudcover_get() {
            owm_hourly_cloudcover_output+=(" | ${lng_str_cloudcover_total:-"Cloud Cover (total):"} ${owm_hourly_cloudcover[$h]:-0} %")
        }

        owm_hourly_cloudcover_low_get() {
            owm_hourly_cloudcover_low_output+=(" | ${lng_str_cloudcover_low:-"Cloud Cover (low):"} $owm_hourly_cloudcover_low %")
        }

        owm_hourly_cloudcover_mid_get() {
            owm_hourly_cloudcover_mid_output+=(" | ${lng_str_cloudcover_mid:-"Cloud Cover (mid):"} ${owm_hourly_cloudcover_mid[$h]:-0} %")
        }

        owm_hourly_cloudcover_high_get() {
            owm_hourly_cloudcover_high_output+=(" | ${lng_str_cloudcover_high:-"Cloud Cover (high):"} ${owm_hourly_cloudcover_high[$h]:-0} %")
        }

        owm_hourly_shortwave_radiation_get() {
            owm_hourly_shortwave_radiation_output+=(" | ${lng_str_shortwave_radiation:-"Shortwave Solar Radiation:"} ${owm_hourly_shortwave_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        owm_hourly_direct_radiation_get() {
            owm_hourly_direct_radiation_output+=(" | ${lng_str_direct_radiation:-"Direct Solar Radiation:"} ${owm_hourly_direct_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        owm_hourly_diffuse_radiation_get() {
            owm_hourly_diffuse_radiation_output+=(" | ${lng_str_diffuse_radiation:-"Diffuse Solar Radiation:"} ${owm_hourly_diffuse_radiation[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        owm_hourly_direct_normal_irradiance_get() {
            owm_hourly_direct_normal_irradiance_output+=(" | ${lng_str_direct_normal_irradiance:-"Direct Normal Irradiance (DNI):"} ${owm_hourly_direct_normal_irradiance[$h]:-0} ${lng_str_unit_wm:-"W/m"}")
        }

        owm_hourly_evapotranspiration_get() {
            owm_hourly_evapotranspiration_output+=(" | ${lng_str_evapotranspiration:-"Evapotranspiration:"} ${owm_hourly_evapotranspiration[$h]:-0} $precip_unit_text")
        }

        owm_hourly_et0_fao_evapotranspiration_get() {
            owm_hourly_et0_fao_evapotranspiration_output+=(" | ${lng_str_et0_fao_evapotranspiration:-"Reference Evapotranspiration (ET₀):"} ${owm_hourly_et0_fao_evapotranspiration[$h]:-0} $precip_unit_text")
        }

        owm_hourly_vapor_pressure_deficit_get() {
            owm_hourly_vapor_pressure_deficit_output+=(" | ${lng_str_vapor_pressure_deficit:-"Vapor Pressure Deficit:"} ${owm_hourly_vapor_pressure_deficit[$h]:-0} ${lng_str_pressure_unit_kpa:-"kPa"}")
        }

        owm_hourly_windspeed_10m_get() {
            owm_hourly_windspeed_10m_output+=(" | ${lng_str_windspeed_10m:-"Wind Speed (10 m):"} ${owm_hourly_windspeed_10m[$h]:-0} $wind_unit_text")
        }

        owm_hourly_windspeed_80m_get() {
            owm_hourly_windspeed_80m_output+=(" | ${lng_str_windspeed_80m:-"Wind Speed (80 m):"} ${owm_hourly_windspeed_80m[$h]:-0} $wind_unit_text")
        }

        owm_hourly_windspeed_120m_get() {
            owm_hourly_windspeed_120m_output+=(" | ${lng_str_windspeed_120m:-"Wind Speed (120 m):"} ${owm_hourly_windspeed_120m[$h]:-0} $wind_unit_text")
        }

        owm_hourly_windspeed_180m_get() {
            owm_hourly_windspeed_180m_output+=(" | ${lng_str_windspeed_180m:-"Wind Speed (180 m):"} ${owm_hourly_windspeed_180m[$h]:-0} $wind_unit_text")
        }

        owm_hourly_winddirection_10m_get() {
            wind_direction=${owm_hourly_winddirection_10m[$h]:-0}
            wind_direction_convertor
            owm_hourly_winddirection_10m_output+=(" | ${lng_str_winddirection_10m:-"Wind Direction (10 m):"} $wind_direction_text")
        }

        owm_hourly_winddirection_80m_get() {
            wind_direction=${owm_hourly_winddirection_80m[$h]:-0}
            wind_direction_convertor
            owm_hourly_winddirection_80m_output+=(" | ${lng_str_winddirection_80m:-"Wind Direction (80 m):"} $wind_direction_text")
        }

        owm_hourly_winddirection_120m_get() {
            wind_direction=${owm_hourly_winddirection_120m[$h]:-0}
            wind_direction_convertor
            owm_hourly_winddirection_120m_output+=(" | ${lng_str_winddirection_120m:-"Wind Direction (120 m):"} $wind_direction_text")
        }

        owm_hourly_winddirection_180m_get() {
            wind_direction=${owm_hourly_winddirection_180m[$h]:-0}
            wind_direction_convertor
            owm_hourly_winddirection_180m_output+=(" | ${lng_str_winddirection_180m:-"Wind Direction (180 m):"} $wind_direction_text")
        }

        owm_hourly_windgusts_10m_get() {
            owm_hourly_windgusts_10m_output+=(" | ${lng_str_windgusts_10m:-"Wind Gusts (10 m):"} ${owm_hourly_windgusts_10m[$h]:-0} $wind_unit_text")
        }

        owm_hourly_soil_temperature_0cm_get() {
            owm_hourly_soil_temperature_0cm_output+=(" | ${lng_str_soil_temperature_0cm:-"Soil Temperature (0 cm):"} ${owm_hourly_soil_temperature_0cm[$h]:-0} $temp_unit_text")
        }

        owm_hourly_soil_temperature_6cm_get() {
            owm_hourly_soil_temperature_6cm_output+=(" | ${lng_str_soil_temperature_6cm:-"Soil Temperature (6 cm):"} ${owm_hourly_soil_temperature_6cm[$h]:-0} $temp_unit_text")
        }

        owm_hourly_soil_temperature_18cm_get() {
            owm_hourly_soil_temperature_18cm_output+=(" | ${lng_str_soil_temperature_18cm:-"Soil Temperature (18 cm):"} ${owm_hourly_soil_temperature_18cm[$h]:-0} $temp_unit_text")
        }

        owm_hourly_soil_temperature_54cm_get() {
            owm_hourly_soil_temperature_54cm_output+=(" | ${lng_str_soil_temperature_54cm:-"Soil Temperature (54 cm):"} ${owm_hourly_soil_temperature_54cm[$h]:-0} $temp_unit_text")
        }

        owm_hourly_soil_moisture_0_1cm_get() {
            owm_hourly_soil_moisture_0_1cm_output+=(" | ${lng_str_soil_moisture_0_1cm:-"Soil Moisture (0-1 cm):"} ${owm_hourly_soil_moisture_0_1cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        owm_hourly_soil_moisture_1_3cm_get() {
            owm_hourly_soil_moisture_1_3cm_output+=(" | ${lng_str_soil_moisture_1_3cm:-"Soil Moisture (1-3 cm):"} ${owm_hourly_soil_moisture_1_3cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        owm_hourly_soil_moisture_3_9cm_get() {
            owm_hourly_soil_moisture_3_9cm_output+=(" | ${lng_str_soil_moisture_3_9cm:-"Soil Moisture (3-9 cm):"} ${owm_hourly_soil_moisture_3_9cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        owm_hourly_soil_moisture_9_27cm_get() {
            owm_hourly_soil_moisture_9_27cm_output+=(" | ${lng_str_soil_moisture_9_27cm:-"Soil Moisture (9-27 cm):"} ${owm_hourly_soil_moisture_9_27cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        owm_hourly_soil_moisture_27_81cm_get() {
            owm_hourly_soil_moisture_27_81cm_output+=(" | ${lng_str_soil_moisture_27_81cm:-"Soil Moisture (27-81 cm):"} ${owm_hourly_soil_moisture_27_81cm[$h]:-0} ${lng_str_unit_m3:-"m³/m³"}")
        }

        # Включенные опции для почасового прогноза погоды
        owm_hourly_options=($owm_hourly_time_enabled $owm_hourly_temperature_2m_enabled $owm_hourly_relativehumidity_2m_enabled $owm_hourly_dewpoint_2m_enabled $owm_hourly_apparent_temperature_enabled $owm_hourly_pressure_msl_enabled $owm_hourly_surface_pressure_enabled $owm_hourly_precipitation_enabled $owm_hourly_rain_enabled $owm_hourly_showers_enabled $owm_hourly_snowfall_enabled $owm_hourly_weathercode_enabled $owm_hourly_snow_depth_enabled $owm_hourly_freezinglevel_height_enabled $owm_hourly_cloudcover_enabled $owm_hourly_cloudcover_low_enabled $owm_hourly_cloudcover_mid_enabled $owm_hourly_cloudcover_high_enabled $owm_hourly_shortwave_radiation_enabled $owm_hourly_direct_radiation_enabled $owm_hourly_diffuse_radiation_enabled $owm_hourly_direct_normal_irradiance_enabled $owm_hourly_evapotranspiration_enabled $owm_hourly_et0_fao_evapotranspiration_enabled $owm_hourly_vapor_pressure_deficit_enabled $owm_hourly_windspeed_10m_enabled $owm_hourly_windspeed_80m_enabled $owm_hourly_windspeed_120m_enabled $owm_hourly_windspeed_180m_enabled $owm_hourly_winddirection_10m_enabled $owm_hourly_winddirection_80m_enabled $owm_hourly_winddirection_120m_enabled $owm_hourly_winddirection_180m_enabled $owm_hourly_windgusts_10m_enabled $owm_hourly_soil_temperature_0cm_enabled $owm_hourly_soil_temperature_6cm_enabled $owm_hourly_soil_temperature_18cm_enabled $owm_hourly_soil_temperature_54cm_enabled $owm_hourly_soil_moisture_0_1cm_enabled $owm_hourly_soil_moisture_1_3cm_enabled $owm_hourly_soil_moisture_3_9cm_enabled $owm_hourly_soil_moisture_9_27cm_enabled $owm_hourly_soil_moisture_27_81cm_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        owm_hourly_options_vallues=(owm_hourly_time_get owm_hourly_temperature_2m_get owm_hourly_relativehumidity_2m_get owm_hourly_dewpoint_2m_get owm_hourly_apparent_temperature_get owm_hourly_pressure_msl_get owm_hourly_surface_pressure_get owm_hourly_precipitation_get owm_hourly_rain_get owm_hourly_showers_get owm_hourly_snowfall_get owm_hourly_weathercode_get owm_hourly_snow_depth_get owm_hourly_freezinglevel_height_get owm_hourly_cloudcover_get owm_hourly_cloudcover_low_get owm_hourly_cloudcover_mid_get owm_hourly_cloudcover_high_get owm_hourly_shortwave_radiation_get owm_hourly_direct_radiation_get owm_hourly_diffuse_radiation_get owm_hourly_direct_normal_irradiance_get owm_hourly_evapotranspiration_get owm_hourly_et0_fao_evapotranspiration_get owm_hourly_vapor_pressure_deficit_get owm_hourly_windspeed_10m_get owm_hourly_windspeed_80m_get owm_hourly_windspeed_120m_get owm_hourly_windspeed_180m_get owm_hourly_winddirection_10m_get owm_hourly_winddirection_80m_get owm_hourly_winddirection_120m_get owm_hourly_winddirection_180m_get owm_hourly_windgusts_10m_get owm_hourly_soil_temperature_0cm_get owm_hourly_soil_temperature_6cm_get owm_hourly_soil_temperature_18cm_get owm_hourly_soil_temperature_54cm_get owm_hourly_soil_moisture_0_1cm_get owm_hourly_soil_moisture_1_3cm_get owm_hourly_soil_moisture_3_9cm_get owm_hourly_soil_moisture_9_27cm_get owm_hourly_soil_moisture_27_81cm_get)

        for om_ho in ${!owm_hourly_options[*]}
        do
            if [[ ${owm_hourly_options[$om_ho]} == "true" ]]
            then
                owm_hourly_options_array+=(${owm_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for owm_hourly_options_array_n in ${!owm_hourly_options_array[*]}
        do
            for ((h=0;h<$owm_hours;h++))
            do
                ${owm_hourly_options_array[$owm_hourly_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по часам
        for ((hr=0;hr<$owm_hours;hr++))
        do
            if (( ${owm_hourly_time[$hr]} > $(date +%s) ))
            then
                echo "|#| OpenWeatherMap${owm_hourly_temperature_2m_output[$hr]}${owm_hourly_relativehumidity_2m_output[$hr]}${owm_hourly_dewpoint_2m_output[$hr]}${owm_hourly_apparent_temperature_output[$hr]}${owm_hourly_pressure_msl_output[$hr]}${owm_hourly_surface_pressure_output[$hr]}${owm_hourly_precipitation_output[$hr]}${owm_hourly_rain_output[$hr]}${owm_hourly_showers_output[$hr]}${owm_hourly_snowfall_output[$hr]}${owm_hourly_weathercode_output[$hr]}${owm_hourly_snow_depth_output[$hr]}${owm_hourly_freezinglevel_height_output[$hr]}${owm_hourly_cloudcover_output[$hr]}${owm_hourly_cloudcover_low_output[$hr]}${owm_hourly_cloudcover_mid_output[$hr]}${owm_hourly_cloudcover_high_output[$hr]}${owm_hourly_shortwave_radiation_output[$hr]}${owm_hourly_direct_radiation_output[$hr]}${owm_hourly_diffuse_radiation_output[$hr]}${owm_hourly_direct_normal_irradiance_output[$hr]}${owm_hourly_evapotranspiration_output[$hr]}${owm_hourly_et0_fao_evapotranspiration_output[$hr]}${owm_hourly_vapor_pressure_deficit_output[$hr]}${owm_hourly_windspeed_10m_output[$hr]}${owm_hourly_windspeed_80m_output[$hr]}${owm_hourly_windspeed_120m_output[$hr]}${owm_hourly_windspeed_180m_output[$hr]}${owm_hourly_winddirection_10m_output[$hr]}${owm_hourly_winddirection_80m_output[$hr]}${owm_hourly_winddirection_120m_output[$hr]}${owm_hourly_winddirection_180m_output[$hr]}${owm_hourly_windgusts_10m_output[$hr]}${owm_hourly_soil_temperature_0cm_output[$hr]}${owm_hourly_soil_temperature_6cm_output[$hr]}${owm_hourly_soil_temperature_18cm_output[$hr]}${owm_hourly_soil_temperature_54cm_output[$hr]}${owm_hourly_soil_moisture_0_1cm_output[$hr]}${owm_hourly_soil_moisture_1_3cm_output[$hr]}${owm_hourly_soil_moisture_3_9cm_output[$hr]}${owm_hourly_soil_moisture_9_27cm_output[$hr]}${owm_hourly_soil_moisture_27_81cm_output[$hr]}${owm_hourly_time_output[$hr]}"
            fi
        done
    }

    # Прогноз по дням OpenWeatherMap
    owm_daily_weather() {
       owm_daily_time_get() {
            owm_daily_time_output+=(" | ${lng_str_date:-"Date:"} $(date -d @${owm_daily_time[$d]:-0} +%d/%m)")
        }

        owm_daily_weathercode_get() {
            owm_weathercode=${owm_daily_weathercode[$d]:-0}
            owm_weather_code_converter
            owm_daily_weathercode_output+=(" | ${lng_str_weather:-"Weather:"} $owm_weathercode_text")
        }

        owm_daily_temperature_2m_max_get() {
            owm_daily_temperature_2m_max_output+=(" | ${lng_str_temperature_2m_max:-"Maximum Temperature (2 m):"} ${owm_daily_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        owm_daily_temperature_2m_min_get() {
            owm_daily_temperature_2m_min_output+=(" | ${lng_str_temperature_2m_min:-"Minimum Temperature (2 m):"} ${owm_daily_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        owm_daily_apparent_temperature_2m_max_get() {
            owm_daily_apparent_temperature_2m_max_output+=(" | ${lng_str_apparent_temperature_2m_max:-"Maximum Apparent Temperature (2 m):"} ${owm_daily_apparent_temperature_2m_max[$d]:-0} $temp_unit_text")
        }

        owm_daily_apparent_temperature_2m_min_get() {
            owm_daily_apparent_temperature_2m_min_output+=(" | ${lng_str_apparent_temperature_2m_min:-"Minimum Apparent Temperature (2 m):"} ${owm_daily_apparent_temperature_2m_min[$d]:-0} $temp_unit_text")
        }

        owm_daily_sunrise_get() {
            owm_daily_sunrise_output+=(" | ${lng_str_sunrise:-"Sunrise:"} $(date -d @${owm_daily_sunrise[$d]:-0} +%H:%M)")
        }

        owm_daily_sunset_get() {
            owm_daily_sunset_output+=(" | ${lng_str_sunset:-"Sunset:"} $(date -d @${owm_daily_sunset[$d]:-0} +%H:%M)")
        }

        owm_daily_precipitation_sum_get() {
            owm_daily_precipitation_sum_output+=(" | ${lng_str_precipitation_sum:-"Precipitation Sum:"} ${owm_daily_precipitation_sum[$d]:-0} $precip_unit_text")
        }

        owm_daily_rain_sum_get() {
            owm_daily_rain_sum_output+=(" | ${lng_str_rain_sum:-"Rain Sum:"} ${owm_daily_rain_sum[$d]:-0} $precip_unit_text")
        }

        owm_daily_showers_sum_get() {
            owm_daily_showers_sum_output+=(" | ${lng_str_showers_sum:-"Showers Sum:"} ${owm_daily_showers_sum[$d]:-0} $precip_unit_text")
        }

        owm_daily_snowfall_sum_get() {
            owm_daily_snowfall_sum_output+=(" | ${lng_str_snowfall_sum:-"Snowfall Sum:"} ${owm_daily_snowfall_sum[$d]:-0} $precip_unit_text")
        }

        owm_daily_precipitation_hours_get() {
            owm_daily_precipitation_hours_output+=(" | ${lng_str_precipitation_hours:-"Precipitation Hours:"} ${owm_daily_precipitation_hours[$d]:-0} ${lng_str_unit_h:-"h"}")
        }

        owm_daily_windspeed_10m_max_get() {
            owm_daily_windspeed_10m_max_output+=(" | ${lng_str_windspeed_10m_max:-"Maximum Wind Speed (10 m):"} ${owm_daily_windspeed_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        owm_daily_windgusts_10m_max_get() {
            owm_daily_windgusts_10m_max_output+=(" | ${lng_str_windgusts_10m_max:-"Maximum Wind Gust (10 m):"} ${owm_daily_windgusts_10m_max[$d]:-0} ${lng_str_wind_unit_ms:-"m/s"}")
        }

        owm_daily_winddirection_10m_dominant_get() {
            owm_daily_winddirection_10m_dominant_default[$d]+=${owm_daily_winddirection_10m_dominant[$d]:-0}
            wind_direction=$((${owm_daily_winddirection_10m_default[$d]%.*}+1))
            wind_direction_convertor
            owm_daily_winddirection_10m_dominant_output+=(" | ${lng_str_winddirection_10m_dominant:-"Dominant Wind Direction (10 m):"} $wind_direction_text")
        }

        owm_daily_shortwave_radiation_sum_get() {
            owm_daily_shortwave_radiation_sum_output+=(" | ${lng_str_shortwave_radiation_sum:-"Shortwave Radiation Sum:"} ${owm_daily_shortwave_radiation_sum[$d]:-0} ${lng_str_unit_mjm2:-"MJ/m²"}")
        }

        owm_daily_et0_fao_evapotranspiration_get() {
            owm_daily_et0_fao_evapotranspiration_output+=(" | ${lng_str_et0_fao_evapotranspiration:-"Reference Evapotranspiration (ET₀):"} ${owm_daily_et0_fao_evapotranspiration[$d]:-0} $precip_unit_text")
        }

        # Опции прогноза погоды по дням
        owm_daily_options=($owm_daily_time_enabled $owm_daily_weathercode_enabled $owm_daily_temperature_2m_max_enabled $owm_daily_temperature_2m_min_enabled $owm_daily_apparent_temperature_2m_max_enabled $owm_daily_apparent_temperature_2m_min_enabled $owm_daily_sunrise_enabled $owm_daily_sunset_enabled $owm_daily_precipitation_sum_enabled $owm_daily_rain_sum_enabled $owm_daily_showers_sum_enabled $owm_daily_snowfall_sum_enabled $owm_daily_precipitation_hours_enabled $owm_daily_windspeed_10m_max_enabled $owm_daily_windgusts_10m_max_enabled $owm_daily_winddirection_10m_dominant_enabled $owm_daily_shortwave_radiation_sum_enabled $owm_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        owm_daily_options_vallues=(owm_daily_time_get owm_daily_weathercode_get owm_daily_temperature_2m_max_get owm_daily_temperature_2m_min_get owm_daily_apparent_temperature_2m_max_get owm_daily_apparent_temperature_2m_min_get owm_daily_sunrise_get owm_daily_sunset_get owm_daily_precipitation_sum_get owm_daily_rain_sum_get owm_daily_showers_sum_get owm_daily_snowfall_sum_get owm_daily_precipitation_hours_get owm_daily_windspeed_10m_max_get owm_daily_windgusts_10m_max_get owm_daily_winddirection_10m_dominant_get owm_daily_shortwave_radiation_sum_get owm_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!owm_daily_options[*]}
        do
            if [[ ${owm_daily_options[$om_do]} == "true" ]]
            then
                owm_daily_options_array+=(${owm_daily_options_vallues[$om_do]})
            fi
        done

        for owm_daily_options_array_n in ${!owm_daily_options_array[*]}
        do
            for ((d=0;d<$owm_days;d++))
            do
                ${owm_daily_options_array[$owm_daily_options_array_n]}
            done
        done

        # Обработка и вывод прогноза по дням
        for ((day=0;day<$owm_days;day++))
        do
            echo "|#| OpenWeatherMap${owm_daily_weathercode_output[$day]}${owm_daily_temperature_2m_max_output[$day]}${owm_daily_temperature_2m_min_output[$day]}${owm_daily_apparent_temperature_2m_max_output[$day]}${owm_daily_apparent_temperature_2m_min_output[$day]}${owm_daily_sunrise_output[$day]}${owm_daily_sunset_output[$day]}${owm_daily_precipitation_sum_output[$day]}${owm_daily_rain_sum_output[$day]}${owm_daily_showers_sum_output[$day]}${owm_daily_snowfall_sum_output[$day]}${owm_daily_precipitation_hours_output[$day]}${owm_daily_windspeed_10m_max_output[$day]}${owm_daily_windgusts_10m_max_output[$day]}${owm_daily_winddirection_10m_dominant_output[$day]}${owm_daily_shortwave_radiation_sum_output[$day]}${owm_daily_et0_fao_evapotranspiration_output[$day]}${owm_daily_time_output[$day]}"
        done
    }

    units

    # Проверка включённых параметров OpenWeatherMap
    owm_array_1=($owm_current_weather_enabled $owm_daily_weather_enabled $owm_hourly_weather_enabled)
    owm_array_2=(owm_current_weather owm_daily_weather owm_hourly_weather)

    for owm_array_X in ${!owm_array_1[*]}
    do
        if [[ ${owm_array_1[$owm_array_X]} == "true" ]]
        then
            ${owm_array_2[$owm_array_X]}
        fi
    done
}

if [[ $owm_current_weather_enabled == "true" || $owm_daily_weather_enabled == "true" || $owm_hourly_weather_enabled == "true" ]]
then
    owm
fi

# Средние значения погоды
wu_avg() {
    # Текущая погода
    wu_avg_current_weather() {
        wu_avg_current_time_get() {
            wu_avg_current_time_output=" | ${lng_str_time:-"Time:"} $(date)"
        }

        wu_avg_current_temperature_get() {
            wu_avg_current_temperature_output=" | ${lng_str_temperature:-"Temperature:"} ${wu_avg_current_temp%.*} $temp_unit_text"
        }

        wu_avg_current_feel_like_temperature_get() {
            wu_avg_current_feel_like_temperature=$(bc -l<<<"13.12 + 0.6215*$wu_avg_current_temp - 11.37*e(l($wu_avg_current_wind_speed*1.5)*0.16) + 0.3965*$wu_avg_current_temp*e(l($wu_avg_current_wind_speed*1.5)*0.16)")
            wu_avg_current_feel_like_temperature_output=" | ${lng_str_feels_like:-"Feels like:"} ${wu_avg_current_feel_like_temperature%.*} $temp_unit_text"
        }

        wu_avg_current_weathercode_get() {
            # Конвертация значения текущей погоды
            #wu_avg_weathercode=$(echo ${wu_avg_current_weather_array[13]:-0} | sed -r 's/_.+//')
            #wu_avg_weather_code_converter
            wu_avg_current_weathercode_output=" | ${lng_str_weather:-"Weather:"} $wu_avg_weathercode_text"
        }

        wu_avg_current_wind_direction_get() {
            wind_direction=$wu_avg_current_wind_direction
            wind_direction_convertor
            wu_avg_current_wind_direction_output=" | ${lng_str_wind_direction:-"Wind Direction:"} $wind_direction_text"
        }

        wu_avg_current_wind_speed_get() {
            wu_avg_current_wind_speed_output=" | ${lng_str_wind_speed:-"Wind Speed:"} ${wu_avg_current_wind_speed%.*} ${lng_str_wind_unit_ms:-"m/s"}"
        }

        # Включенные опции для почасового прогноза погоды
        wu_avg_current_options=($wu_avg_current_time_enabled $wu_avg_current_temperature_enabled $wu_avg_current_feel_like_temperature_enabled $wu_avg_current_weathercode_enabled $wu_avg_current_wind_direction_enabled $wu_avg_current_wind_speed_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        wu_avg_current_options_vallues=(wu_avg_current_time_get wu_avg_current_temperature_get wu_avg_current_feel_like_temperature_get wu_avg_current_weathercode_get wu_avg_current_wind_direction_get wu_avg_current_wind_speed_get)

        for wu_avg_current in ${!wu_avg_current_options[*]}
        do
            if [[ ${wu_avg_current_options[$wu_avg_current]} == "true" ]]
            then
                wu_avg_current_options_array+=(${wu_avg_current_options_vallues[$wu_avg_current]})
            fi
        done

        # Обработка включенных опций
        for wu_avg_current_options_array_n in ${!wu_avg_current_options_array[*]}
        do
            ${wu_avg_current_options_array[$wu_avg_current_options_array_n]}
        done

        # Вывод текущей погоды
        echo "|#| ${lng_str_avg_current_weather:-"Average current weather"} | ${lng_str_current_weather:-"Current weather"}$wu_avg_current_temperature_output$wu_avg_current_feel_like_temperature_output$wu_avg_current_wind_speed_output$wu_avg_current_wind_direction_output$wu_avg_current_weathercode_output$wu_avg_current_time_output"
    }

    # Проверка включённых параметров OpenWeatherMap
    wu_avg_array_1=($wu_avg_current_weather_enabled $wu_avg_daily_weather_enabled $wu_avg_hourly_weather_enabled)
    wu_avg_array_2=(wu_avg_current_weather wu_avg_daily_weather wu_avg_hourly_weather)

    for wu_avg_array_X in ${!wu_avg_array_1[*]}
    do
        if [[ ${wu_avg_array_1[$wu_avg_array_X]} == "true" ]]
        then
            ${wu_avg_array_2[$wu_avg_array_X]}
        fi
    done
}



# Проверка условий для включения средних значений погоды
if [[ $wu_avg_current_weather_enabled == "true" || $wu_avg_daily_weather_enabled == "true" || $wu_avg_hourly_weather_enabled == "true" ]]
then
    if [[ $open_meteo_current_weather_enabled == "true" && $met_no_current_weather_enabled == "true" || $open_meteo_current_weather_enabled == "true" && $owm_current_weather_enabled == "true" || $met_no_current_weather_enabled == "true" && $owm_current_weather_enabled == "true" ]]
    then
        wu_avg
    fi
fi
