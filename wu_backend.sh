#!/bin/bash

# Загрузка настроек из файла конфигурации
if [ -e "$(pwd)/wu.cfg" ]
then
    wu_cfg_file="$(pwd)/wu.cfg"
    . $wu_cfg_file
else
    if [ -e "$(dirname "$0")/wu.cfg" ]
    then
        wu_cfg_file="$(dirname "$0")/wu.cfg"
        . $wu_cfg_file
    else
        if [ -e "$HOME/.config/WeatherUniverse/wu.cfg" ]
        then
            wu_cfg_file="$HOME/.config/WeatherUniverse/wu.cfg"
            . $wu_cfg_file
        else
            if [ -e "$HOME/.local/share/WeatherUniverse/wu.cfg" ]
            then
                wu_cfg_file="$HOME/.local/share/WeatherUniverse/wu.cfg"
                . $wu_cfg_file
            fi
        fi
    fi
fi

# Загрузка файла локализации
if [ -e "$(pwd)/locales/backend/$wu_language.ini" ]
then
    . "$(pwd)/locales/backend/$wu_language.ini"
else
    if [ -e "$(dirname "$0")/locales/backend/$wu_language.ini" ]
    then
        . "$(dirname "$0")/locales/backend/$wu_language.ini"
    else
        if [ -e "$HOME/.config/WeatherUniverse/locales/backend/$wu_language.ini" ]
        then
            . "$HOME/.config/WeatherUniverse/locales/backend/$wu_language.ini"
        else
            if [ -e "$HOME/.local/share/WeatherUniverse/locales/backend/$wu_language.ini" ]
            then
                . "$HOME/.local/share/WeatherUniverse/locales/backend/$wu_language.ini"
            fi
        fi
    fi
fi

# Определение загрузчика данных
if [[ $data_downloader == "Auto" ]]
then
    if command -v wget
    then
        data_downloader="wget -qO-"
        sed -i 's/data_downloader=.*/data_downloader="wget -qO-"/' $wu_cfg_file
        clear
    else
        if command -v curl
        then
            data_downloader="curl -s"
            sed -i 's/data_downloader=.*/data_downloader="curl -s"/' $wu_cfg_file
            clear
        else
            echo ${lng_str_wu_no_data_downloader:-"You do not have any of the supported downloaders installed. Install wget or curl to continue, or specify your own in the configuration file!"}
            exit
        fi
    fi
fi

# Определение обработчика json данных
if [[ $json_processor == "Auto" ]]
then
    if command -v gojq
    then
        json_processor="gojq -r"
        sed -i 's/json_processor=.*/json_processor="gojq -r"/' $wu_cfg_file
        clear
    else
        if command -v jq
        then
            json_processor="jq -r"
            sed -i 's/json_processor=.*/json_processor="jq -r"/' $wu_cfg_file
            clear
        else
            echo ${lng_str_wu_no_json_processor:-"You do not have a supported json parser installed. Install gojq or jq to continue, or specify your own in the configuration file!"}
            exit
        fi
    fi
fi

# Open-Meteo провайдер
open_meteo() {
    # Текущая погода Open-Meteo
    open_meteo_current_weather() {
        # Получение текущей погоды
        open_meteo_current_weather_array=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor '.current_weather.temperature, .current_weather.windspeed, .current_weather.winddirection, .current_weather.weathercode, .current_weather.time'))
    }

    # Прогноз по часам Open-Meteo
    open_meteo_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        open_meteo_hourly_time_get() {
            open_meteo_hourly_time+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.time[$h]"))
        }

        open_meteo_hourly_temperature_2m_get() {
            open_meteo_hourly_temperature_2m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.temperature_2m[$h]"))
        }

        open_meteo_hourly_relativehumidity_2m_get() {
            open_meteo_hourly_relativehumidity_2m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.relativehumidity_2m[$h]"))
        }

        open_meteo_hourly_dewpoint_2m_get() {
            open_meteo_hourly_dewpoint_2m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.dewpoint_2m[$h]"))
        }

        open_meteo_hourly_apparent_temperature_get() {
            open_meteo_hourly_apparent_temperature+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.apparent_temperature[$h]"))
        }

        open_meteo_hourly_pressure_msl_get() {
            open_meteo_hourly_pressure_msl+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.pressure_msl[$h]"))
        }

        open_meteo_hourly_surface_pressure_get() {
            open_meteo_hourly_surface_pressure+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.surface_pressure[$h]"))
        }

        open_meteo_hourly_precipitation_get() {
            open_meteo_hourly_precipitation+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.precipitation[$h]"))
        }

        open_meteo_hourly_rain_get() {
            open_meteo_hourly_rain+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.rain[$h]"))
        }

        open_meteo_hourly_showers_get() {
            open_meteo_hourly_showers+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.showers[$h]"))
        }

        open_meteo_hourly_snowfall_get() {
            open_meteo_hourly_snowfall+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.snowfall[$h]"))
        }

        open_meteo_hourly_weathercode_get() {
            open_meteo_hourly_weathercode+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.weathercode[$h]"))
        }

        open_meteo_hourly_snow_depth_get() {
            open_meteo_hourly_snow_depth+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.snow_depth[$h]"))
        }

        open_meteo_hourly_freezinglevel_height_get() {
            open_meteo_hourly_freezinglevel_height+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.freezinglevel_height[$h]"))
        }

        open_meteo_hourly_cloudcover_get() {
            open_meteo_hourly_cloudcover+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.cloudcover[$h]"))
        }

        open_meteo_hourly_cloudcover_low_get() {
            open_meteo_hourly_cloudcover_low+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.cloudcover_low[$h]"))
        }

        open_meteo_hourly_cloudcover_mid_get() {
            open_meteo_hourly_cloudcover_mid+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.cloudcover_mid[$h]"))
        }

        open_meteo_hourly_cloudcover_high_get() {
            open_meteo_hourly_cloudcover_high+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.cloudcover_high[$h]"))
        }

        open_meteo_hourly_shortwave_radiation_get() {
            open_meteo_hourly_shortwave_radiation+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.shortwave_radiation[$h]"))
        }

        open_meteo_hourly_direct_radiation_get() {
            open_meteo_hourly_direct_radiation+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.direct_radiation[$h]"))
        }

        open_meteo_hourly_diffuse_radiation_get() {
            open_meteo_hourly_diffuse_radiation+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.diffuse_radiation[$h]"))
        }

        open_meteo_hourly_direct_normal_irradiance_get() {
            open_meteo_hourly_direct_normal_irradiance+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.direct_normal_irradiance[$h]"))
        }

        open_meteo_hourly_evapotranspiration_get() {
            open_meteo_hourly_evapotranspiration+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.evapotranspiration[$h]"))
        }

        open_meteo_hourly_et0_fao_evapotranspiration_get() {
            open_meteo_hourly_et0_fao_evapotranspiration+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.et0_fao_evapotranspiration[$h]"))
        }

        open_meteo_hourly_vapor_pressure_deficit_get() {
            open_meteo_hourly_vapor_pressure_deficit+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.vapor_pressure_deficit[$h]"))
        }

        open_meteo_hourly_windspeed_10m_get() {
            open_meteo_hourly_windspeed_10m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.windspeed_10m[$h]"))
        }

        open_meteo_hourly_windspeed_80m_get() {
            open_meteo_hourly_windspeed_80m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.windspeed_80m[$h]"))
        }

        open_meteo_hourly_windspeed_120m_get() {
            open_meteo_hourly_windspeed_120m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.windspeed_120m[$h]"))
        }

        open_meteo_hourly_windspeed_180m_get() {
            open_meteo_hourly_windspeed_180m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.windspeed_180m[$h]"))
        }

        open_meteo_hourly_winddirection_10m_get() {
            open_meteo_hourly_winddirection_10m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.winddirection_10m[$h]"))
        }

        open_meteo_hourly_winddirection_80m_get() {
            open_meteo_hourly_winddirection_80m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.winddirection_80m[$h]"))
        }

        open_meteo_hourly_winddirection_120m_get() {
            open_meteo_hourly_winddirection_120m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.winddirection_120m[$h]"))
        }

        open_meteo_hourly_winddirection_180m_get() {
            open_meteo_hourly_winddirection_180m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.winddirection_180m[$h]"))
        }

        open_meteo_hourly_windgusts_10m_get() {
            open_meteo_hourly_windgusts_10m+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.windgusts_10m[$h]"))
        }

        open_meteo_hourly_soil_temperature_0cm_get() {
            open_meteo_hourly_soil_temperature_0cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_temperature_0cm[$h]"))
        }

        open_meteo_hourly_soil_temperature_6cm_get() {
            open_meteo_hourly_soil_temperature_6cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_temperature_6cm[$h]"))
        }

        open_meteo_hourly_soil_temperature_18cm_get() {
            open_meteo_hourly_soil_temperature_18cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_temperature_18cm[$h]"))
        }

        open_meteo_hourly_soil_temperature_54cm_get() {
            open_meteo_hourly_soil_temperature_54cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_temperature_54cm[$h]"))
        }

        open_meteo_hourly_soil_moisture_0_1cm_get() {
            open_meteo_hourly_soil_moisture_0_1cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_moisture_0_1cm[$h]"))
        }

        open_meteo_hourly_soil_moisture_1_3cm_get() {
            open_meteo_hourly_soil_moisture_1_3cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_moisture_1_3cm[$h]"))
        }

        open_meteo_hourly_soil_moisture_3_9cm_get() {
            open_meteo_hourly_soil_moisture_3_9cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_moisture_3_9cm[$h]"))
        }

        open_meteo_hourly_soil_moisture_9_27cm_get() {
            open_meteo_hourly_soil_moisture_9_27cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_moisture_9_27cm[$h]"))
        }

        open_meteo_hourly_soil_moisture_27_81cm_get() {
            open_meteo_hourly_soil_moisture_27_81cm+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".hourly.soil_moisture_27_81cm[$h]"))
        }

        # Включенные опции для почасового прогноза погоды
        open_meteo_hourly_options=($open_meteo_hourly_time_enabled $open_meteo_hourly_temperature_2m_enabled $open_meteo_hourly_relativehumidity_2m_enabled $open_meteo_hourly_dewpoint_2m_enabled $open_meteo_hourly_apparent_temperature_enabled $open_meteo_hourly_pressure_msl_enabled $open_meteo_hourly_surface_pressure_enabled $open_meteo_hourly_precipitation_enabled $open_meteo_hourly_rain_enabled $open_meteo_hourly_showers_enabled $open_meteo_hourly_snowfall_enabled $open_meteo_hourly_weathercode_enabled $open_meteo_hourly_snow_depth_enabled $open_meteo_hourly_freezinglevel_height_enabled $open_meteo_hourly_cloudcover_enabled $open_meteo_hourly_cloudcover_low_enabled $open_meteo_hourly_cloudcover_mid_enabled $open_meteo_hourly_cloudcover_high_enabled $open_meteo_hourly_shortwave_radiation_enabled $open_meteo_hourly_direct_radiation_enabled $open_meteo_hourly_diffuse_radiation_enabled $open_meteo_hourly_direct_normal_irradiance_enabled $open_meteo_hourly_evapotranspiration_enabled $open_meteo_hourly_et0_fao_evapotranspiration_enabled $open_meteo_hourly_vapor_pressure_deficit_enabled $open_meteo_hourly_windspeed_10m_enabled $open_meteo_hourly_windspeed_80m_enabled $open_meteo_hourly_windspeed_120m_enabled $open_meteo_hourly_windspeed_180m_enabled $open_meteo_hourly_winddirection_10m_enabled $open_meteo_hourly_winddirection_80m_enabled $open_meteo_hourly_winddirection_120m_enabled $open_meteo_hourly_winddirection_180m_enabled $open_meteo_hourly_windgusts_10m_enabled $open_meteo_hourly_soil_temperature_0cm_enabled $open_meteo_hourly_soil_temperature_6cm_enabled $open_meteo_hourly_soil_temperature_18cm_enabled $open_meteo_hourly_soil_temperature_54cm_enabled $open_meteo_hourly_soil_moisture_0_1cm_enabled $open_meteo_hourly_soil_moisture_1_3cm_enabled $open_meteo_hourly_soil_moisture_3_9cm_enabled $open_meteo_hourly_soil_moisture_9_27cm_enabled $open_meteo_hourly_soil_moisture_27_81cm_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        open_meteo_hourly_options_vallues=(open_meteo_hourly_time_get open_meteo_hourly_temperature_2m_get open_meteo_hourly_relativehumidity_2m_get open_meteo_hourly_dewpoint_2m_get open_meteo_hourly_apparent_temperature_get open_meteo_hourly_pressure_msl_get open_meteo_hourly_surface_pressure_get open_meteo_hourly_precipitation_get open_meteo_hourly_rain_get open_meteo_hourly_showers_get open_meteo_hourly_snowfall_get open_meteo_hourly_weathercode_get open_meteo_hourly_snow_depth_get open_meteo_hourly_freezinglevel_height_get open_meteo_hourly_cloudcover_get open_meteo_hourly_cloudcover_low_get open_meteo_hourly_cloudcover_mid_get open_meteo_hourly_cloudcover_high_get open_meteo_hourly_shortwave_radiation_get open_meteo_hourly_direct_radiation_get open_meteo_hourly_diffuse_radiation_get open_meteo_hourly_direct_normal_irradiance_get open_meteo_hourly_evapotranspiration_get open_meteo_hourly_et0_fao_evapotranspiration_get open_meteo_hourly_vapor_pressure_deficit_get open_meteo_hourly_windspeed_10m_get open_meteo_hourly_windspeed_80m_get open_meteo_hourly_windspeed_120m_get open_meteo_hourly_windspeed_180m_get open_meteo_hourly_winddirection_10m_get open_meteo_hourly_winddirection_80m_get open_meteo_hourly_winddirection_120m_get open_meteo_hourly_winddirection_180m_get open_meteo_hourly_windgusts_10m_get open_meteo_hourly_soil_temperature_0cm_get open_meteo_hourly_soil_temperature_6cm_get open_meteo_hourly_soil_temperature_18cm_get open_meteo_hourly_soil_temperature_54cm_get open_meteo_hourly_soil_moisture_0_1cm_get open_meteo_hourly_soil_moisture_1_3cm_get open_meteo_hourly_soil_moisture_3_9cm_get open_meteo_hourly_soil_moisture_9_27cm_get open_meteo_hourly_soil_moisture_27_81cm_get)

        for om_ho in ${!open_meteo_hourly_options[*]}
        do
            if [[ ${open_meteo_hourly_options[$om_ho]} == "true" ]]
            then
                open_meteo_hourly_options_array+=(${open_meteo_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for ((h=0;h<$open_meteo_hours;h++))
        do
            for open_meteo_hourly_options_array_n in ${!open_meteo_hourly_options_array[*]}
            do
                ${open_meteo_hourly_options_array[$open_meteo_hourly_options_array_n]}
            done
            # Увеличение количества часов для соответствия заданным параметрам, если время прогноза меньше текущего времени
            if (( ${open_meteo_hourly_time[$h]} < $(date +%s) && open_meteo_hours < 167 ))
            then
                (( open_meteo_hours++ ))
            fi
        done
    }

    # Прогноз по дням Open-Meteo
    open_meteo_daily_weather() {
       open_meteo_daily_time_get() {
            open_meteo_daily_time+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.time[$d]"))
        }

        open_meteo_daily_weathercode_get() {
            open_meteo_daily_weathercode+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.weathercode[$d]"))
        }

        open_meteo_daily_temperature_2m_max_get() {
            open_meteo_daily_temperature_2m_max+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.temperature_2m_max[$d]"))
        }

        open_meteo_daily_temperature_2m_min_get() {
            open_meteo_daily_temperature_2m_min+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.temperature_2m_min[$d]"))
        }

        open_meteo_daily_apparent_temperature_2m_max_get() {
            open_meteo_daily_apparent_temperature_2m_max+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.apparent_temperature_max[$d]"))
        }

        open_meteo_daily_apparent_temperature_2m_min_get() {
            open_meteo_daily_apparent_temperature_2m_min+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.apparent_temperature_min[$d]"))
        }

        open_meteo_daily_sunrise_get() {
            open_meteo_daily_sunrise+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.sunrise[$d]"))
        }

        open_meteo_daily_sunset_get() {
            open_meteo_daily_sunset+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.sunset[$d]"))
        }

        open_meteo_daily_precipitation_sum_get() {
            open_meteo_daily_precipitation_sum+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.precipitation_sum[$d]"))
        }

        open_meteo_daily_rain_sum_get() {
            open_meteo_daily_rain_sum+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.rain_sum[$d]"))
        }

        open_meteo_daily_showers_sum_get() {
            open_meteo_daily_showers_sum+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.showers_sum[$d]"))
        }

        open_meteo_daily_snowfall_sum_get() {
            open_meteo_daily_snowfall_sum+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.snowfall_sum[$d]"))
        }

        open_meteo_daily_precipitation_hours_get() {
            open_meteo_daily_precipitation_hours+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.precipitation_hours[$d]"))
        }

        open_meteo_daily_windspeed_10m_max_get() {
            open_meteo_daily_windspeed_10m_max+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.windspeed_10m_max[$d]"))
        }

        open_meteo_daily_windgusts_10m_max_get() {
            open_meteo_daily_windgusts_10m_max+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.windgusts_10m_max[$d]"))
        }

        open_meteo_daily_winddirection_10m_dominant_get() {
            open_meteo_daily_winddirection_10m_dominant+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.winddirection_10m_dominant[$d]"))
        }

        open_meteo_daily_shortwave_radiation_sum_get() {
            open_meteo_daily_shortwave_radiation_sum+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.shortwave_radiation_sum[$d]"))
        }

        open_meteo_daily_et0_fao_evapotranspiration_get() {
            open_meteo_daily_et0_fao_evapotranspiration+=($(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor ".daily.et0_fao_evapotranspiration[$d]"))
        }

        # Опции прогноза погоды по дням
        open_meteo_daily_options=($open_meteo_daily_time_enabled $open_meteo_daily_weathercode_enabled $open_meteo_daily_temperature_2m_max_enabled $open_meteo_daily_temperature_2m_min_enabled $open_meteo_daily_apparent_temperature_2m_max_enabled $open_meteo_daily_apparent_temperature_2m_min_enabled $open_meteo_daily_sunrise_enabled $open_meteo_daily_sunset_enabled $open_meteo_daily_precipitation_sum_enabled $open_meteo_daily_rain_sum_enabled $open_meteo_daily_showers_sum_enabled $open_meteo_daily_snowfall_sum_enabled $open_meteo_daily_precipitation_hours_enabled $open_meteo_daily_windspeed_10m_max_enabled $open_meteo_daily_windgusts_10m_max_enabled $open_meteo_daily_winddirection_10m_dominant_enabled $open_meteo_daily_shortwave_radiation_sum_enabled $open_meteo_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        open_meteo_daily_options_vallues=(open_meteo_daily_time_get open_meteo_daily_weathercode_get open_meteo_daily_temperature_2m_max_get open_meteo_daily_temperature_2m_min_get open_meteo_daily_apparent_temperature_2m_max_get open_meteo_daily_apparent_temperature_2m_min_get open_meteo_daily_sunrise_get open_meteo_daily_sunset_get open_meteo_daily_precipitation_sum_get open_meteo_daily_rain_sum_get open_meteo_daily_showers_sum_get open_meteo_daily_snowfall_sum_get open_meteo_daily_precipitation_hours_get open_meteo_daily_windspeed_10m_max_get open_meteo_daily_windgusts_10m_max_get open_meteo_daily_winddirection_10m_dominant_get open_meteo_daily_shortwave_radiation_sum_get open_meteo_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!open_meteo_daily_options[*]}
        do
            if [[ ${open_meteo_daily_options[$om_do]} == "true" ]]
            then
                open_meteo_daily_options_array+=(${open_meteo_daily_options_vallues[$om_do]})
            fi
        done

        for open_meteo_daily_options_array_n in ${!open_meteo_daily_options_array[*]}
        do
            for ((d=0;d<$open_meteo_days;d++))
            do
                ${open_meteo_daily_options_array[$open_meteo_daily_options_array_n]}
            done
        done
    }

    # Проверка параметров Open-Meteo
    open_meteo_checker() {
        open_meteo_array_1=($open_meteo_current_weather_enabled $open_meteo_daily_weather_enabled $open_meteo_hourly_weather_enabled)
        open_meteo_array_2=(open_meteo_current_weather open_meteo_daily_weather open_meteo_hourly_weather)

        for open_meteo_array_x in ${!open_meteo_array_1[*]}
        do
            if [[ ${open_meteo_array_1[$open_meteo_array_x]} == "true" ]]
            then
                ${open_meteo_array_2[$open_meteo_array_x]}
            fi
        done
    }

    # Ограничение обновления файла данных - один раз в час
    open_meteo_update_limit() {
        if (( ($(date +%s) - $(cat "$wu_data_files/open_meteo/open_meteo_data.json" | $json_processor '.current_weather.time')) < 3600 ))
        then
            open_meteo_checker
        else
            mv $wu_data_files/open_meteo/open_meteo_data.json $wu_data_files/open_meteo/old/open_meteo_data_$(date -d "today" +$filetimeformat).json
            $data_downloader "$open_meteo_api_url" > $wu_data_files/open_meteo/open_meteo_data.json && open_meteo_checker
        fi
    }

    # Проверка наличия файла данных Open-Meteo
    open_meteo_check_file_exists() {
        if [ -e $wu_data_files/open_meteo/open_meteo_data.json ]
        then
            open_meteo_update_limit
        else
            $data_downloader "$open_meteo_api_url" > $wu_data_files/open_meteo/open_meteo_data.json && open_meteo_checker
        fi
    }

    open_meteo_check_file_exists

}

if [[ $open_meteo_current_weather_enabled == "true" || $open_meteo_daily_weather_enabled == "true" || $open_meteo_hourly_weather_enabled == "true" ]]
then
    open_meteo
fi


# MI провайдер
met_no() {
    # Текущая погода MI
    met_no_current_weather() {
        # Получение текущей погоды
        met_no_current_weather_array=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor '.properties.timeseries[0].time, .properties.timeseries[0].data.instant.details.air_pressure_at_sea_level, .properties.timeseries[0].data.instant.details.air_temperature, .properties.timeseries[0].data.instant.details.cloud_area_fraction, .properties.timeseries[0].data.instant.details.cloud_area_fraction_high, .properties.timeseries[0].data.instant.details.cloud_area_fraction_low, .properties.timeseries[0].data.instant.details.cloud_area_fraction_medium, .properties.timeseries[0].data.instant.details.dew_point_temperature, .properties.timeseries[0].data.instant.details.fog_area_fraction, .properties.timeseries[0].data.instant.details.relative_humidity, .properties.timeseries[0].data.instant.details.ultraviolet_index_clear_sky, .properties.timeseries[0].data.instant.details.wind_from_direction, .properties.timeseries[0].data.instant.details.wind_speed, .properties.timeseries[0].data.next_1_hours.summary.symbol_code, .properties.timeseries[0].data.next_1_hours.details.precipitation_amount'))
    }

    # Прогноз по часам MI
    met_no_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        met_no_hourly_time_get() {
            met_no_hourly_time+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].time"))
        }
        
        met_no_hourly_air_pressure_at_sea_level_get() {
            met_no_hourly_air_pressure_at_sea_level+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.air_pressure_at_sea_level"))
        }
        
        met_no_hourly_air_temperature_get() {
            met_no_hourly_air_temperature+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.air_temperature"))
        }

        met_no_hourly_cloud_area_fraction_get() {
            met_no_hourly_cloud_area_fraction+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.cloud_area_fraction"))
        }
        
        met_no_hourly_cloud_area_fraction_low_get() {
            met_no_hourly_cloud_area_fraction_low+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.cloud_area_fraction_low"))
        }
        
        met_no_hourly_cloud_area_fraction_medium_get() {
            met_no_hourly_cloud_area_fraction_medium+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.cloud_area_fraction_medium"))
        }
        
        met_no_hourly_cloud_area_fraction_high_get() {
            met_no_hourly_cloud_area_fraction_high+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.cloud_area_fraction_high"))
        }
        
        met_no_hourly_dew_point_temperature_get() {
            met_no_hourly_dew_point_temperature+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.dew_point_temperature"))
        }

        met_no_hourly_fog_area_fraction_get() {
            met_no_hourly_fog_area_fraction+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.fog_area_fraction"))
        }
        
        met_no_hourly_precipitation_amount_get() {
            met_no_hourly_precipitation_amount+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.next_1_hours.details.precipitation_amount"))
        }
        
        met_no_hourly_relative_humidity_get() {
            met_no_hourly_relative_humidity+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.relative_humidity"))
        }

        met_no_hourly_ultraviolet_index_clear_sky_get() {
            met_no_hourly_ultraviolet_index_clear_sky+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.ultraviolet_index_clear_sky"))
        }
        
        met_no_hourly_wind_from_direction_get() {
            met_no_hourly_wind_from_direction+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.wind_from_direction"))
        }
        
        met_no_hourly_wind_speed_get() {
            met_no_hourly_wind_speed+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.instant.details.wind_speed"))
        }
        
        met_no_hourly_weathercode_get() {
            met_no_hourly_weathercode+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".properties.timeseries[$h].data.next_1_hours.summary.symbol_code"))
        }

        # Включенные опции для почасового прогноза погоды
        met_no_hourly_options=($met_no_hourly_time_enabled $met_no_hourly_air_pressure_at_sea_level_enabled $met_no_hourly_air_temperature_enabled $met_no_hourly_cloud_area_fraction_enabled $met_no_hourly_cloud_area_fraction_low_enabled $met_no_hourly_cloud_area_fraction_medium_enabled $met_no_hourly_cloud_area_fraction_high_enabled $met_no_hourly_dew_point_temperature_enabled $met_no_hourly_fog_area_fraction_enabled $met_no_hourly_precipitation_amount_enabled $met_no_hourly_relative_humidity_enabled $met_no_hourly_ultraviolet_index_clear_sky_enabled $met_no_hourly_wind_from_direction_enabled $met_no_hourly_wind_speed_enabled $met_no_hourly_weathercode_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        met_no_hourly_options_vallues=(met_no_hourly_time_get met_no_hourly_air_pressure_at_sea_level_get met_no_hourly_air_temperature_get met_no_hourly_cloud_area_fraction_get met_no_hourly_cloud_area_fraction_low_get met_no_hourly_cloud_area_fraction_medium_get met_no_hourly_cloud_area_fraction_high_get met_no_hourly_dew_point_temperature_get met_no_hourly_fog_area_fraction_get met_no_hourly_precipitation_amount_get met_no_hourly_relative_humidity_get met_no_hourly_ultraviolet_index_clear_sky_get met_no_hourly_wind_from_direction_get met_no_hourly_wind_speed_get met_no_hourly_weathercode_get)

        for om_ho in ${!met_no_hourly_options[*]}
        do
            if [[ ${met_no_hourly_options[$om_ho]} == "true" ]]
            then
                met_no_hourly_options_array+=(${met_no_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for ((h=0;h<$met_no_hours;h++))
        do
            for met_no_hourly_options_array_n in ${!met_no_hourly_options_array[*]}
            do
                ${met_no_hourly_options_array[$met_no_hourly_options_array_n]}
            done

            if (( $(date +%s -d ${met_no_hourly_time[$h]}) < $(date +%s) && met_no_hours < 90 ))
            then
                (( met_no_hours++ ))
            fi
        done
    }

    # Прогноз по дням MI
    met_no_daily_weather() {
       met_no_daily_time_get() {
            met_no_daily_time+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.time[$d]"))
        }

        met_no_daily_weathercode_get() {
            met_no_daily_weathercode+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.weathercode[$d]"))
        }

        met_no_daily_temperature_2m_max_get() {
            met_no_daily_temperature_2m_max+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.temperature_2m_max[$d]"))
        }

        met_no_daily_temperature_2m_min_get() {
            met_no_daily_temperature_2m_min+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.temperature_2m_min[$d]"))
        }

        met_no_daily_apparent_temperature_2m_max_get() {
            met_no_daily_apparent_temperature_2m_max+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.apparent_temperature_max[$d]"))
        }

        met_no_daily_apparent_temperature_2m_min_get() {
            met_no_daily_apparent_temperature_2m_min+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.apparent_temperature_min[$d]"))
        }

        met_no_daily_sunrise_get() {
            met_no_daily_sunrise+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.sunrise[$d]"))
        }

        met_no_daily_sunset_get() {
            met_no_daily_sunset+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.sunset[$d]"))
        }

        met_no_daily_precipitation_sum_get() {
            met_no_daily_precipitation_sum+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.precipitation_sum[$d]"))
        }

        met_no_daily_rain_sum_get() {
            met_no_daily_rain_sum+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.rain_sum[$d]"))
        }

        met_no_daily_showers_sum_get() {
            met_no_daily_showers_sum+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.showers_sum[$d]"))
        }

        met_no_daily_snowfall_sum_get() {
            met_no_daily_snowfall_sum+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.snowfall_sum[$d]"))
        }

        met_no_daily_precipitation_hours_get() {
            met_no_daily_precipitation_hours+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.precipitation_hours[$d]"))
        }

        met_no_daily_windspeed_10m_max_get() {
            met_no_daily_windspeed_10m_max+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.windspeed_10m_max[$d]"))
        }

        met_no_daily_windgusts_10m_max_get() {
            met_no_daily_windgusts_10m_max+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.windgusts_10m_max[$d]"))
        }

        met_no_daily_winddirection_10m_dominant_get() {
            met_no_daily_winddirection_10m_dominant+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.winddirection_10m_dominant[$d]"))
        }

        met_no_daily_shortwave_radiation_sum_get() {
            met_no_daily_shortwave_radiation_sum+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.shortwave_radiation_sum[$d]"))
        }

        met_no_daily_et0_fao_evapotranspiration_get() {
            met_no_daily_et0_fao_evapotranspiration+=($(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor ".daily.et0_fao_evapotranspiration[$d]"))
        }

        # Опции прогноза погоды по дням
        met_no_daily_options=($met_no_daily_time_enabled $met_no_daily_weathercode_enabled $met_no_daily_temperature_2m_max_enabled $met_no_daily_temperature_2m_min_enabled $met_no_daily_apparent_temperature_2m_max_enabled $met_no_daily_apparent_temperature_2m_min_enabled $met_no_daily_sunrise_enabled $met_no_daily_sunset_enabled $met_no_daily_precipitation_sum_enabled $met_no_daily_rain_sum_enabled $met_no_daily_showers_sum_enabled $met_no_daily_snowfall_sum_enabled $met_no_daily_precipitation_hours_enabled $met_no_daily_windspeed_10m_max_enabled $met_no_daily_windgusts_10m_max_enabled $met_no_daily_winddirection_10m_dominant_enabled $met_no_daily_shortwave_radiation_sum_enabled $met_no_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        met_no_daily_options_vallues=(met_no_daily_time_get met_no_daily_weathercode_get met_no_daily_temperature_2m_max_get met_no_daily_temperature_2m_min_get met_no_daily_apparent_temperature_2m_max_get met_no_daily_apparent_temperature_2m_min_get met_no_daily_sunrise_get met_no_daily_sunset_get met_no_daily_precipitation_sum_get met_no_daily_rain_sum_get met_no_daily_showers_sum_get met_no_daily_snowfall_sum_get met_no_daily_precipitation_hours_get met_no_daily_windspeed_10m_max_get met_no_daily_windgusts_10m_max_get met_no_daily_winddirection_10m_dominant_get met_no_daily_shortwave_radiation_sum_get met_no_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!met_no_daily_options[*]}
        do
            if [[ ${met_no_daily_options[$om_do]} == "true" ]]
            then
                met_no_daily_options_array+=(${met_no_daily_options_vallues[$om_do]})
            fi
        done

        for met_no_daily_options_array_n in ${!met_no_daily_options_array[*]}
        do
            for ((d=0;d<$met_no_days;d++))
            do
                ${met_no_daily_options_array[$met_no_daily_options_array_n]}
            done
        done
    }

    # Проверка параметров MI
    met_no_checker() {
        met_no_array_1=($met_no_current_weather_enabled $met_no_daily_weather_enabled $met_no_hourly_weather_enabled)
        met_no_array_2=(met_no_current_weather met_no_daily_weather met_no_hourly_weather)

        for met_no_array_x in ${!met_no_array_1[*]}
        do
            if [[ ${met_no_array_1[$met_no_array_x]} == "true" ]]
            then
                ${met_no_array_2[$met_no_array_x]}
            fi
        done
    }

    # Ограничение обновления файла данных - один раз в час
    met_no_update_limit() {
        if (( ($(date +%s) - $(date +%s -d $(cat "$wu_data_files/met_no/met_no_data.json" | $json_processor -r '.properties.timeseries[0].time'))) < 5400 ))
        then
            met_no_checker
        else
            mv $wu_data_files/met_no/met_no_data.json $wu_data_files/met_no/old/met_no_data_$(date -d "today" +$filetimeformat).json
            $data_downloader "$met_no_api_url" > $wu_data_files/met_no/met_no_data.json && met_no_checker
        fi
    }

    # Проверка наличия файла данных MI
    met_no_check_file_exists() {
        if [ -e $wu_data_files/met_no/met_no_data.json ]
        then
            met_no_update_limit
        else
            $data_downloader "$met_no_api_url" > $wu_data_files/met_no/met_no_data.json && met_no_checker
        fi
    }

    met_no_check_file_exists

}

if [[ $met_no_current_weather_enabled == "true" || $met_no_daily_weather_enabled == "true" || $met_no_hourly_weather_enabled == "true" ]]
then
    met_no
fi

# OpenWeatherMap провайдер
owm() {
    # Текущая погода OpenWeatherMap
    owm_current_weather() {
            owm_current_weather=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor '.dt, .sys.sunrise, .sys.sunset, .main.temp, .main.temp_min, .main.temp_max, .main.feels_like, .main.pressure, .main.humidity, .clouds.all, .visibility, .wind.speed, .wind.deg, .wind.gust, .weather[0].id'))
    }

    # Прогноз по часам OpenWeatherMap
    owm_hourly_weather() {
        # Функции вывода информации для каждого элемента почасового прогноза
        owm_hourly_time_get() {
            owm_hourly_time+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.time[$h]"))
        }

        owm_hourly_temperature_2m_get() {
            owm_hourly_temperature_2m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.temperature_2m[$h]"))
        }

        owm_hourly_relativehumidity_2m_get() {
            owm_hourly_relativehumidity_2m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.relativehumidity_2m[$h]"))
        }

        owm_hourly_dewpoint_2m_get() {
            owm_hourly_dewpoint_2m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.dewpoint_2m[$h]"))
        }

        owm_hourly_apparent_temperature_get() {
            owm_hourly_apparent_temperature+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.apparent_temperature[$h]"))
        }

        owm_hourly_pressure_msl_get() {
            owm_hourly_pressure_msl+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.pressure_msl[$h]"))
        }

        owm_hourly_surface_pressure_get() {
            owm_hourly_surface_pressure+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.surface_pressure[$h]"))
        }

        owm_hourly_precipitation_get() {
            owm_hourly_precipitation+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.precipitation[$h]"))
        }

        owm_hourly_rain_get() {
            owm_hourly_rain+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.rain[$h]"))
        }

        owm_hourly_showers_get() {
            owm_hourly_showers+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.showers[$h]"))
        }

        owm_hourly_snowfall_get() {
            owm_hourly_snowfall+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.snowfall[$h]"))
        }

        owm_hourly_weathercode_get() {
            owm_hourly_weathercode+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.weathercode[$h]"))
        }

        owm_hourly_snow_depth_get() {
            owm_hourly_snow_depth+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.snow_depth[$h]"))
        }

        owm_hourly_freezinglevel_height_get() {
            owm_hourly_freezinglevel_height+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.freezinglevel_height[$h]"))
        }

        owm_hourly_cloudcover_get() {
            owm_hourly_cloudcover+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.cloudcover[$h]"))
        }

        owm_hourly_cloudcover_low_get() {
            owm_hourly_cloudcover_low+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.cloudcover_low[$h]"))
        }

        owm_hourly_cloudcover_mid_get() {
            owm_hourly_cloudcover_mid+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.cloudcover_mid[$h]"))
        }

        owm_hourly_cloudcover_high_get() {
            owm_hourly_cloudcover_high+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.cloudcover_high[$h]"))
        }

        owm_hourly_shortwave_radiation_get() {
            owm_hourly_shortwave_radiation+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.shortwave_radiation[$h]"))
        }

        owm_hourly_direct_radiation_get() {
            owm_hourly_direct_radiation+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.direct_radiation[$h]"))
        }

        owm_hourly_diffuse_radiation_get() {
            owm_hourly_diffuse_radiation+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.diffuse_radiation[$h]"))
        }

        owm_hourly_direct_normal_irradiance_get() {
            owm_hourly_direct_normal_irradiance+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.direct_normal_irradiance[$h]"))
        }

        owm_hourly_evapotranspiration_get() {
            owm_hourly_evapotranspiration+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.evapotranspiration[$h]"))
        }

        owm_hourly_et0_fao_evapotranspiration_get() {
            owm_hourly_et0_fao_evapotranspiration+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.et0_fao_evapotranspiration[$h]"))
        }

        owm_hourly_vapor_pressure_deficit_get() {
            owm_hourly_vapor_pressure_deficit+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.vapor_pressure_deficit[$h]"))
        }

        owm_hourly_windspeed_10m_get() {
            owm_hourly_windspeed_10m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.windspeed_10m[$h]"))
        }

        owm_hourly_windspeed_80m_get() {
            owm_hourly_windspeed_80m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.windspeed_80m[$h]"))
        }

        owm_hourly_windspeed_120m_get() {
            owm_hourly_windspeed_120m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.windspeed_120m[$h]"))
        }

        owm_hourly_windspeed_180m_get() {
            owm_hourly_windspeed_180m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.windspeed_180m[$h]"))
        }

        owm_hourly_winddirection_10m_get() {
            owm_hourly_winddirection_10m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.winddirection_10m[$h]"))
        }

        owm_hourly_winddirection_80m_get() {
            owm_hourly_winddirection_80m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.winddirection_80m[$h]"))
        }

        owm_hourly_winddirection_120m_get() {
            owm_hourly_winddirection_120m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.winddirection_120m[$h]"))
        }

        owm_hourly_winddirection_180m_get() {
            owm_hourly_winddirection_180m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.winddirection_180m[$h]"))
        }

        owm_hourly_windgusts_10m_get() {
            owm_hourly_windgusts_10m+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.windgusts_10m[$h]"))
        }

        owm_hourly_soil_temperature_0cm_get() {
            owm_hourly_soil_temperature_0cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_temperature_0cm[$h]"))
        }

        owm_hourly_soil_temperature_6cm_get() {
            owm_hourly_soil_temperature_6cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_temperature_6cm[$h]"))
        }

        owm_hourly_soil_temperature_18cm_get() {
            owm_hourly_soil_temperature_18cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_temperature_18cm[$h]"))
        }

        owm_hourly_soil_temperature_54cm_get() {
            owm_hourly_soil_temperature_54cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_temperature_54cm[$h]"))
        }

        owm_hourly_soil_moisture_0_1cm_get() {
            owm_hourly_soil_moisture_0_1cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_moisture_0_1cm[$h]"))
        }

        owm_hourly_soil_moisture_1_3cm_get() {
            owm_hourly_soil_moisture_1_3cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_moisture_1_3cm[$h]"))
        }

        owm_hourly_soil_moisture_3_9cm_get() {
            owm_hourly_soil_moisture_3_9cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_moisture_3_9cm[$h]"))
        }

        owm_hourly_soil_moisture_9_27cm_get() {
            owm_hourly_soil_moisture_9_27cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_moisture_9_27cm[$h]"))
        }

        owm_hourly_soil_moisture_27_81cm_get() {
            owm_hourly_soil_moisture_27_81cm+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".hourly.soil_moisture_27_81cm[$h]"))
        }

        # Включенные опции для почасового прогноза погоды
        owm_hourly_options=($owm_hourly_time_enabled $owm_hourly_temperature_2m_enabled $owm_hourly_relativehumidity_2m_enabled $owm_hourly_dewpoint_2m_enabled $owm_hourly_apparent_temperature_enabled $owm_hourly_pressure_msl_enabled $owm_hourly_surface_pressure_enabled $owm_hourly_precipitation_enabled $owm_hourly_rain_enabled $owm_hourly_showers_enabled $owm_hourly_snowfall_enabled $owm_hourly_weathercode_enabled $owm_hourly_snow_depth_enabled $owm_hourly_freezinglevel_height_enabled $owm_hourly_cloudcover_enabled $owm_hourly_cloudcover_low_enabled $owm_hourly_cloudcover_mid_enabled $owm_hourly_cloudcover_high_enabled $owm_hourly_shortwave_radiation_enabled $owm_hourly_direct_radiation_enabled $owm_hourly_diffuse_radiation_enabled $owm_hourly_direct_normal_irradiance_enabled $owm_hourly_evapotranspiration_enabled $owm_hourly_et0_fao_evapotranspiration_enabled $owm_hourly_vapor_pressure_deficit_enabled $owm_hourly_windspeed_10m_enabled $owm_hourly_windspeed_80m_enabled $owm_hourly_windspeed_120m_enabled $owm_hourly_windspeed_180m_enabled $owm_hourly_winddirection_10m_enabled $owm_hourly_winddirection_80m_enabled $owm_hourly_winddirection_120m_enabled $owm_hourly_winddirection_180m_enabled $owm_hourly_windgusts_10m_enabled $owm_hourly_soil_temperature_0cm_enabled $owm_hourly_soil_temperature_6cm_enabled $owm_hourly_soil_temperature_18cm_enabled $owm_hourly_soil_temperature_54cm_enabled $owm_hourly_soil_moisture_0_1cm_enabled $owm_hourly_soil_moisture_1_3cm_enabled $owm_hourly_soil_moisture_3_9cm_enabled $owm_hourly_soil_moisture_9_27cm_enabled $owm_hourly_soil_moisture_27_81cm_enabled)

        # Значения для сопоставления включенных опций почасового прогноза погоды
        owm_hourly_options_vallues=(owm_hourly_time_get owm_hourly_temperature_2m_get owm_hourly_relativehumidity_2m_get owm_hourly_dewpoint_2m_get owm_hourly_apparent_temperature_get owm_hourly_pressure_msl_get owm_hourly_surface_pressure_get owm_hourly_precipitation_get owm_hourly_rain_get owm_hourly_showers_get owm_hourly_snowfall_get owm_hourly_weathercode_get owm_hourly_snow_depth_get owm_hourly_freezinglevel_height_get owm_hourly_cloudcover_get owm_hourly_cloudcover_low_get owm_hourly_cloudcover_mid_get owm_hourly_cloudcover_high_get owm_hourly_shortwave_radiation_get owm_hourly_direct_radiation_get owm_hourly_diffuse_radiation_get owm_hourly_direct_normal_irradiance_get owm_hourly_evapotranspiration_get owm_hourly_et0_fao_evapotranspiration_get owm_hourly_vapor_pressure_deficit_get owm_hourly_windspeed_10m_get owm_hourly_windspeed_80m_get owm_hourly_windspeed_120m_get owm_hourly_windspeed_180m_get owm_hourly_winddirection_10m_get owm_hourly_winddirection_80m_get owm_hourly_winddirection_120m_get owm_hourly_winddirection_180m_get owm_hourly_windgusts_10m_get owm_hourly_soil_temperature_0cm_get owm_hourly_soil_temperature_6cm_get owm_hourly_soil_temperature_18cm_get owm_hourly_soil_temperature_54cm_get owm_hourly_soil_moisture_0_1cm_get owm_hourly_soil_moisture_1_3cm_get owm_hourly_soil_moisture_3_9cm_get owm_hourly_soil_moisture_9_27cm_get owm_hourly_soil_moisture_27_81cm_get)

        for om_ho in ${!owm_hourly_options[*]}
        do
            if [[ ${owm_hourly_options[$om_ho]} == "true" ]]
            then
                owm_hourly_options_array+=(${owm_hourly_options_vallues[$om_ho]})
            fi
        done

        # Обработка включенных опций
        for ((h=0;h<$owm_hours;h++))
        do
            for owm_hourly_options_array_n in ${!owm_hourly_options_array[*]}
            do
                ${owm_hourly_options_array[$owm_hourly_options_array_n]}
            done

            if (( ${owm_hourly_time[$h]} < $(date +%s) && owm_hours < 167 ))
            then
                (( owm_hours++ ))
            fi
        done
    }

    # Прогноз по дням OpenWeatherMap
    owm_daily_weather() {
       owm_daily_time_get() {
            owm_daily_time+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.time[$d]"))
        }

        owm_daily_weathercode_get() {
            owm_daily_weathercode+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.weathercode[$d]"))
        }

        owm_daily_temperature_2m_max_get() {
            owm_daily_temperature_2m_max+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.temperature_2m_max[$d]"))
        }

        owm_daily_temperature_2m_min_get() {
            owm_daily_temperature_2m_min+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.temperature_2m_min[$d]"))
        }

        owm_daily_apparent_temperature_2m_max_get() {
            owm_daily_apparent_temperature_2m_max+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.apparent_temperature_max[$d]"))
        }

        owm_daily_apparent_temperature_2m_min_get() {
            owm_daily_apparent_temperature_2m_min+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.apparent_temperature_min[$d]"))
        }

        owm_daily_sunrise_get() {
            owm_daily_sunrise+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.sunrise[$d]"))
        }

        owm_daily_sunset_get() {
            owm_daily_sunset+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.sunset[$d]"))
        }

        owm_daily_precipitation_sum_get() {
            owm_daily_precipitation_sum+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.precipitation_sum[$d]"))
        }

        owm_daily_rain_sum_get() {
            owm_daily_rain_sum+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.rain_sum[$d]"))
        }

        owm_daily_showers_sum_get() {
            owm_daily_showers_sum+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.showers_sum[$d]"))
        }

        owm_daily_snowfall_sum_get() {
            owm_daily_snowfall_sum+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.snowfall_sum[$d]"))
        }

        owm_daily_precipitation_hours_get() {
            owm_daily_precipitation_hours+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.precipitation_hours[$d]"))
        }

        owm_daily_windspeed_10m_max_get() {
            owm_daily_windspeed_10m_max+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.windspeed_10m_max[$d]"))
        }

        owm_daily_windgusts_10m_max_get() {
            owm_daily_windgusts_10m_max+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.windgusts_10m_max[$d]"))
        }

        owm_daily_winddirection_10m_dominant_get() {
            owm_daily_winddirection_10m_dominant+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.winddirection_10m_dominant[$d]"))
        }

        owm_daily_shortwave_radiation_sum_get() {
            owm_daily_shortwave_radiation_sum+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.shortwave_radiation_sum[$d]"))
        }

        owm_daily_et0_fao_evapotranspiration_get() {
            owm_daily_et0_fao_evapotranspiration+=($(cat "$wu_data_files/owm/owm_data.json" | $json_processor ".daily.et0_fao_evapotranspiration[$d]"))
        }

        # Опции прогноза погоды по дням
        owm_daily_options=($owm_daily_time_enabled $owm_daily_weathercode_enabled $owm_daily_temperature_2m_max_enabled $owm_daily_temperature_2m_min_enabled $owm_daily_apparent_temperature_2m_max_enabled $owm_daily_apparent_temperature_2m_min_enabled $owm_daily_sunrise_enabled $owm_daily_sunset_enabled $owm_daily_precipitation_sum_enabled $owm_daily_rain_sum_enabled $owm_daily_showers_sum_enabled $owm_daily_snowfall_sum_enabled $owm_daily_precipitation_hours_enabled $owm_daily_windspeed_10m_max_enabled $owm_daily_windgusts_10m_max_enabled $owm_daily_winddirection_10m_dominant_enabled $owm_daily_shortwave_radiation_sum_enabled $owm_daily_et0_fao_evapotranspiration_enabled)

        # Значения для сопоставления включенных опций прогноза погоды по дням
        owm_daily_options_vallues=(owm_daily_time_get owm_daily_weathercode_get owm_daily_temperature_2m_max_get owm_daily_temperature_2m_min_get owm_daily_apparent_temperature_2m_max_get owm_daily_apparent_temperature_2m_min_get owm_daily_sunrise_get owm_daily_sunset_get owm_daily_precipitation_sum_get owm_daily_rain_sum_get owm_daily_showers_sum_get owm_daily_snowfall_sum_get owm_daily_precipitation_hours_get owm_daily_windspeed_10m_max_get owm_daily_windgusts_10m_max_get owm_daily_winddirection_10m_dominant_get owm_daily_shortwave_radiation_sum_get owm_daily_et0_fao_evapotranspiration_get)

        for om_do in ${!owm_daily_options[*]}
        do
            if [[ ${owm_daily_options[$om_do]} == "true" ]]
            then
                owm_daily_options_array+=(${owm_daily_options_vallues[$om_do]})
            fi
        done

        for owm_daily_options_array_n in ${!owm_daily_options_array[*]}
        do
            for ((d=0;d<$owm_days;d++))
            do
                ${owm_daily_options_array[$owm_daily_options_array_n]}
            done
        done
    }

    # Проверка параметров OpenWeatherMap
    owm_checker() {
        owm_array_1=($owm_current_weather_enabled $owm_daily_weather_enabled $owm_hourly_weather_enabled)
        owm_array_2=(owm_current_weather owm_daily_weather owm_hourly_weather)

        for owm_array_x in ${!owm_array_1[*]}
        do
            if [[ ${owm_array_1[$owm_array_x]} == "true" ]]
            then
                ${owm_array_2[$owm_array_x]}
            fi
        done
    }

    # Ограничение обновления файла данных - один раз в час
    owm_update_limit() {
        if (( ($(date +%s) - $(cat "$wu_data_files/owm/owm_data.json" | $json_processor '.dt')) < 3600 ))
        then
            owm_checker
        else
            mv $wu_data_files/owm/owm_data.json $wu_data_files/owm/old/owm_data_$(date -d "today" +$filetimeformat).json
            $data_downloader "$owm_api_url" > $wu_data_files/owm/owm_data.json && owm_checker
        fi
    }

    # Проверка наличия файла данных OpenWeatherMap
    owm_check_file_exists() {
        if [ -e $wu_data_files/owm/owm_data.json ]
        then
            owm_update_limit
        else
            $data_downloader "$owm_api_url" > $wu_data_files/owm/owm_data.json && owm_checker
        fi
    }

    owm_check_file_exists

}

if [[ $owm_current_weather_enabled == "true" || $owm_daily_weather_enabled == "true" || $owm_hourly_weather_enabled == "true" ]]
then
    owm
fi

# Средние значения погоды
wu_avg() {
    providers_array=($open_meteo_current_weather_enabled $met_no_current_weather_enabled $owm_current_weather_enabled)

    for providers_array_x in ${!providers_array[*]}
    do
        if [[ ${providers_array[$providers_array_x]} == "true" ]]
        then
            ((providers_num++))
        fi
    done

    met_no_current_wind_direction_int=${met_no_current_weather_array[11]%.*}

    wu_avg_current_temp=$(bc<<<"scale=2;(${open_meteo_current_weather_array[0]:-0}+${met_no_current_weather_array[2]:-0}+${owm_current_weather[3]:-0})/$providers_num")
    wu_avg_current_wind_speed=$(bc<<<"scale=2;(${open_meteo_current_weather_array[1]:-0}+${met_no_current_weather_array[12]:-0}+${owm_current_weather[11]:-0})/$providers_num")
    wu_avg_current_wind_direction=$(((${open_meteo_current_weather_array[2]:-0}+${met_no_current_wind_direction_int:-0}+${owm_current_weather[12]:-0})/$providers_num))
}

# Проверка условий для включения средних значений погоды
if [[ $wu_avg_current_weather_enabled == "true" ]]
then
    if [[ $open_meteo_current_weather_enabled == "true" && $met_no_current_weather_enabled == "true" || $open_meteo_current_weather_enabled == "true" && $owm_current_weather_enabled == "true" || $met_no_current_weather_enabled == "true" && $owm_current_weather_enabled == "true" ]]
    then
        wu_avg
    fi
fi
